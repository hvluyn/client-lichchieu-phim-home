package com.cg.repository;


import com.cg.domain.entity.StaffAvatar;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StaffAvatarRepository extends JpaRepository<StaffAvatar, Long> {

}
