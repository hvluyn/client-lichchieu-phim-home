package com.cg.service.movieavatar;

import com.cg.domain.entity.MovieAvatar;
import com.cg.service.IGeneralService;

public interface IMovieAvatarService extends IGeneralService<MovieAvatar> {

}
