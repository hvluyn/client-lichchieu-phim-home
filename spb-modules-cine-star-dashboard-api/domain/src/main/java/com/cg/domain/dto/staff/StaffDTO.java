package com.cg.domain.dto.staff;

import com.cg.domain.entity.LocationRegion;
import com.cg.domain.entity.Staff;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
public class StaffDTO {
    private Long id;

    private String fullName;

    private String email;

    private Date dob;
    private String phone;

    private LocationRegion locationRegion;

    private String idAvatar;

    private String fileName;
    private String fileFolder;
    private String fileUrl;

    public Staff toStaff(StaffDTO staffDTO) {
        return new Staff()
                .setId(id)
                .setFullName(fullName)
                .setEmail(email)
                .setPhone(phone)
                .setDob(dob)
                .setLocationRegion(locationRegion)
                ;
    }
}
