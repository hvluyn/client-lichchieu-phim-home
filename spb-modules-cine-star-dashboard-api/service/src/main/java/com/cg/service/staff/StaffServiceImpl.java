package com.cg.service.staff;

import com.cg.cloudinary.CloudinaryUploadUtil;
import com.cg.domain.dto.customer.CustomerDTO;
import com.cg.domain.dto.staff.StaffDTO;
import com.cg.domain.dto.staff.StaffRequestDTO;
import com.cg.domain.entity.LocationRegion;
import com.cg.domain.entity.Staff;
import com.cg.domain.entity.StaffAvatar;
import com.cg.exception.DataInputException;
import com.cg.repository.LocationRegionRepository;
import com.cg.repository.ShowRepository;
import com.cg.repository.StaffAvatarRepository;
import com.cg.repository.StaffRepository;
import com.cg.service.upload.IUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional
public class StaffServiceImpl implements IStaffService {

    @Autowired
    private StaffRepository staffRepository;
    @Autowired
    private CloudinaryUploadUtil uploadUtil;

    @Autowired
    private IUploadService uploadService;

    @Autowired
    private StaffAvatarRepository staffAvatarRepository;
    @Autowired
    private LocationRegionRepository locationRegionRepository;

    @Override
    public List<Staff> findAll() {
        return staffRepository.findAll();
    }

    @Override
    public Staff getById(Long id) {
        return staffRepository.getById(id);
    }

    @Override
    public Optional<Staff> findById(Long id) {
        return staffRepository.findById(id);
    }

    @Override
    public Staff save(Staff staff) {
        return staffRepository.save(staff);
    }

    @Override
    public void remove(Long id) {

    }

    @Override
    public List<StaffDTO> findAllStaffResponseDTO() {
        return staffRepository.findAllStaffResponseDTO();
    }

    @Override
    public StaffDTO findStaffResponseDTOById(Long id) {
        return staffRepository.findStaffResponseDTOById(id);
    }

    @Override
    public Boolean existsStaffByEmailAndIdNot(String email, Long id) {
        return staffRepository.existsStaffByEmailAndIdNot(email, id);
    }

    @Override
    public Boolean existsStaffByEmail(String email) {
         return staffRepository.existsStaffByEmail(email);
    }

    @Override
    public Staff update(StaffRequestDTO staffRequestDTO) {
        Staff newStaff = staffRequestDTO.toStaff(staffRequestDTO);
        Optional<Staff> staffOptional = findById(staffRequestDTO.getId());
        if (!staffOptional.isPresent()) {
            throw new DataInputException("Nhân viên không tồn tại");
        }
        Staff staff = staffOptional.get();

        staff.setFullName(staffRequestDTO.getFullName());
        staff.setEmail(staffRequestDTO.getEmail());
        try {
            staff.setDob(new SimpleDateFormat("yyyy-MM-dd").parse(staffRequestDTO.getDob()));
        } catch (Exception e) {
            throw new DataInputException("Dữ liệu không hợp lệ");
        }
        LocationRegion locationRegion=newStaff.getLocationRegion();
        locationRegion.setId(staff.getLocationRegion().getId());
        staff.setLocationRegion(locationRegion);
//        staff.setUser(user);
        staffRepository.save(staff);
        if (staffRequestDTO.getFile() == null) {
            return staff;
        }
        String fileType = staffRequestDTO.getFile().getContentType();
        assert fileType != null;
        fileType = fileType.substring(0, 5);


        StaffAvatar newStaffAvatar = new StaffAvatar();
        newStaffAvatar.setId(null);
        newStaffAvatar.setFileType(fileType);
        StaffAvatar staffAvatar = staff.getStaffAvatar();
        staffAvatarRepository.delete(staffAvatar);
        staffAvatarRepository.save(newStaffAvatar);
        uploadAndSaveStaffImage(staffRequestDTO, newStaffAvatar);
        staff.setStaffAvatar(newStaffAvatar);
        return staffRepository.save(staff);
    }

    @Override
    public Staff create(StaffRequestDTO staffRequestDTO) {
        Staff staff = staffRequestDTO.toStaff(staffRequestDTO);
        staff.setId(null);
        locationRegionRepository.save(staff.getLocationRegion());
        staffRepository.save(staff);

        String fileType = staffRequestDTO.getFile().getContentType();

        assert fileType != null;

        fileType = fileType.substring(0, 5);

        StaffAvatar staffAvatar = new StaffAvatar();
        staffAvatar.setId(null);
        staffAvatar.setFileType(fileType);
        staffAvatarRepository.save(staffAvatar);
        uploadAndSaveStaffImage(staffRequestDTO, staffAvatar);
        staff.setStaffAvatar(staffAvatar);
        return staff;
    }

    private void uploadAndSaveStaffImage(StaffRequestDTO staffRequestDTO, StaffAvatar staffAvatar) {
        try {
            Map uploadResult = uploadService.uploadImage(staffRequestDTO.getFile(), uploadUtil.buildImageUploadParams(staffAvatar.getId(), uploadUtil.STAFF_IMAGE_UPLOAD_FOLDER, uploadUtil.ERROR_IMAGE_UPLOAD));
            String fileUrl = (String) uploadResult.get("secure_url");
            String fileFormat = (String) uploadResult.get("format");

            staffAvatar.setFileName(staffAvatar.getId() + "." + fileFormat);
            staffAvatar.setFileUrl(fileUrl);
            staffAvatar.setFileFolder(uploadUtil.STAFF_IMAGE_UPLOAD_FOLDER);
            staffAvatar.setCloudId(staffAvatar.getFileFolder() + "/" + staffAvatar.getId());
            staffAvatarRepository.save(staffAvatar);

        } catch (IOException e) {
            e.printStackTrace();
            throw new DataInputException("Upload hình ảnh thất bại");
        }
    }

    @Override
    public void softDelete(Long staffId) {
        staffRepository.softDelete(staffId);
    }
}
