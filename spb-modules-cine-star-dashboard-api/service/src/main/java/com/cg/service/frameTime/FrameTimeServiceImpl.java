//package com.cg.service.frameTime;
//
//import com.cg.repository.FrameTimeRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//import java.util.Optional;
//
//@Service
//@Transactional
//public class FrameTimeServiceImpl implements IFrameTimeService {
//
//    @Autowired
//    private FrameTimeRepository frameTimeRepository;
//
//    @Override
//    public List<FrameTime> findAll() {
//        return frameTimeRepository.findAll();
//    }
//
//    @Override
//    public FrameTime getById(Long id) {
//        return frameTimeRepository.getById(id);
//    }
//
//    @Override
//    public Optional<FrameTime> findById(Long id) {
//        return frameTimeRepository.findById(id);
//    }
//
//
//
//    @Override
//    public FrameTime save(FrameTime frameTime) {
//        return frameTimeRepository.save(frameTime);
//    }
//
//    @Override
//
//   public void remove(Long id) {
//
//    }
//}
