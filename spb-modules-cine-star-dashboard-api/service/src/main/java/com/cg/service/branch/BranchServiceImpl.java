//package com.cg.service.branch;
//
//import com.cg.domain.entity.Branch;
//import com.cg.repository.BranchRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//import java.util.Optional;
//
//@Service
//@Transactional
//public class BranchServiceImpl implements IBranchService {
//
//    @Autowired
//    private BranchRepository branchRepository;
//
//    @Override
//    public List<Branch> findAll() {
//        return branchRepository.findAll();
//    }
//
//    @Override
//    public Branch getById(Long id) {
//        return branchRepository.getById(id);
//    }
//
//    @Override
//    public Optional<Branch> findById(Long id) {
//        return branchRepository.findById(id);
//    }
//
//
//
//    @Override
//    public Branch save(Branch branch) {
//        return branchRepository.save(branch);
//    }
//
//    @Override
//
//   public void remove(Long id) {
//
//    }
//}
