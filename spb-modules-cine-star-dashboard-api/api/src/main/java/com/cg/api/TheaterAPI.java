package com.cg.api;

import com.cg.domain.dto.showtimes.ShowTimeDTO;
import com.cg.domain.entity.Theater;
import com.cg.service.showtime.IShowtimeService;
import com.cg.service.theater.ITheaterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/theaters")
public class TheaterAPI {

    @Autowired
    private ITheaterService theaterService ;
    @Autowired
    private IShowtimeService  showtimeService ;

    @GetMapping
    public ResponseEntity<?> getAllTheater() {
        List<Theater> theaters= theaterService.findAll();
        return new ResponseEntity<>(theaters, HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getAllTheaterByMovieId(@PathVariable Long id) {
        List<Theater> theaters= showtimeService.findTheaterByMovieId(id);
        return new ResponseEntity<>(theaters, HttpStatus.OK);
    }

    @GetMapping("/movies/{id}")
    public ResponseEntity<?> getAllMovieAndShowTimeByTheaterId(@PathVariable Long id) {
        List<Theater> theaters= showtimeService.findTheaterByMovieId(id);
        return new ResponseEntity<>(theaters, HttpStatus.OK);
    }
}
