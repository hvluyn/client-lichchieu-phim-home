//package com.cg.service.branchAvatar;
//
//import com.cg.domain.entity.BranchAvatar;
//import com.cg.repository.BranchAvatarRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//import java.util.Optional;
//
//@Service
//@Transactional
//public class BranchAvatarServiceImpl implements IBranchAvatarService {
//
//    @Autowired
//    private BranchAvatarRepository branchAvatarRepository;
//
//    @Override
//    public List<BranchAvatar> findAll() {
//        return branchAvatarRepository.findAll();
//    }
//
//    @Override
//    public BranchAvatar getById(Long id) {
//        return branchAvatarRepository.getById(id);
//    }
//
//    @Override
//    public Optional<BranchAvatar> findById(Long id) {
//        return branchAvatarRepository.findById(id);
//    }
//
//
//
//    @Override
//    public BranchAvatar save(BranchAvatar branchAvatar) {
//        return branchAvatarRepository.save(branchAvatar);
//    }
//
//    @Override
//
//   public void remove(Long id) {
//
//    }
//}
