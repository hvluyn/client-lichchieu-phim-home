package com.cg.domain.dto.showtimes;

import com.cg.domain.entity.Movie;
import com.cg.domain.entity.Room;
import com.cg.domain.entity.Theater;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
public class ShowTimeRequest {

    private Long id;


    private Long movieId;


    private Long roomId;


    private String frameTime;

    private String dayShow;

    private Long theaterId;
}
