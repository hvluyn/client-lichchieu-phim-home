package com.cg.service.country;

import com.cg.domain.entity.Country;
import com.cg.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CountryServiceImpl implements ICountryService {

    @Autowired
    private CountryRepository countryRepository;

    @Override
    public List<Country> findAll() {
        return countryRepository.findAll();
    }

    @Override
    public Country getById(Long id) {
        return countryRepository.getById(id);
    }

    @Override
    public Optional<Country> findById(Long id) {
        return countryRepository.findById(id);
    }



    @Override
    public Country save(Country country) {
        return countryRepository.save(country);
    }

    @Override

   public void remove(Long id) {

    }
}
