package com.cg.service.billTicketItem;

import com.cg.domain.entity.BillTicketItem;
import com.cg.repository.BillTicketItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class BillTicketItemServiceImpl implements IBillTicketItemService {

    @Autowired
    private BillTicketItemRepository billTicketItemRepository;

    @Override
    public List<BillTicketItem> findAll() {
        return billTicketItemRepository.findAll();
    }

    @Override
    public BillTicketItem getById(Long id) {
        return billTicketItemRepository.getById(id);
    }

    @Override
    public Optional<BillTicketItem> findById(Long id) {
        return billTicketItemRepository.findById(id);
    }



    @Override
    public BillTicketItem save(BillTicketItem billTicketItem) {
        return billTicketItemRepository.save(billTicketItem);
    }

    @Override

   public void remove(Long id) {

    }
}
