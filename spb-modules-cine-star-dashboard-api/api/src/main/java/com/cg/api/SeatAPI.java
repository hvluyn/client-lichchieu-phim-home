package com.cg.api;

import com.cg.domain.entity.Room;
import com.cg.domain.entity.SeatType;
import com.cg.domain.entity.Theater;
import com.cg.exception.DataInputException;
import com.cg.service.seatType.ISeatTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/seats")
public class SeatAPI {
    @Autowired
    private ISeatTypeService seatTypeService;


    @GetMapping
    public ResponseEntity<?> getAllSeatTypes() {
        List<SeatType> seatTypes= seatTypeService.findAll();
        return new ResponseEntity<>(seatTypes, HttpStatus.OK);
    }
}
