package com.cg.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Accessors(chain = true)
public class BillFoodItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "food_id", referencedColumnName = "id", nullable = false)
    private Food food;

    @Column(name = "food_title")
    private String productTitle;

    @Column(name = "food_price", precision = 12, scale = 0, nullable = false)
    private BigDecimal foodPrice;

    @Column(name = "food_quantity")
    private Long foodQuantity;

    @Column(name = "food_amount", precision = 12, scale = 0, nullable = false)
    private BigDecimal foodAmount;

    @ManyToOne
    @JoinColumn(name = "bill_id", referencedColumnName = "id", nullable = false)
    private Bill bill;
}
