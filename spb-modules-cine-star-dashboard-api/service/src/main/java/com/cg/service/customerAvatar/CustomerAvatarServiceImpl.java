package com.cg.service.customerAvatar;

import com.cg.domain.entity.CustomerAvatar;
import com.cg.repository.CustomerAvatarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CustomerAvatarServiceImpl implements ICustomerAvatarService {

    @Autowired
    private CustomerAvatarRepository customerAvatarRepository;

    @Override
    public List<CustomerAvatar> findAll() {
        return customerAvatarRepository.findAll();
    }

    @Override
    public CustomerAvatar getById(Long id) {
        return customerAvatarRepository.getById(id);
    }

    @Override
    public Optional<CustomerAvatar> findById(Long id) {
        return customerAvatarRepository.findById(id);
    }



    @Override
    public CustomerAvatar save(CustomerAvatar customerAvatar) {
        return customerAvatarRepository.save(customerAvatar);
    }

    @Override

   public void remove(Long id) {

    }
}
