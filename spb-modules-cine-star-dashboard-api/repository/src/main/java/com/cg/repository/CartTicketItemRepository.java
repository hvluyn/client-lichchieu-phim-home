package com.cg.repository;

import com.cg.domain.entity.CartTicketItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartTicketItemRepository extends JpaRepository<CartTicketItem, Long> {
}
