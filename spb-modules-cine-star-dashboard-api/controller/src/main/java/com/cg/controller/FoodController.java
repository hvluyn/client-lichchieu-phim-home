package com.cg.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/food")
public class FoodController {
    @GetMapping
    public String showIndexPage() {
        return "cp/food/list";
    }

    @GetMapping("/create")
    public String showcreatePage() {
        return "cp/food/createFood";
    }
}
