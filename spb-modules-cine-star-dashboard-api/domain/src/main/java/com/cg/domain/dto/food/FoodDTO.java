package com.cg.domain.dto.food;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
public class FoodDTO {


    private Long id;

    private String name;

    private String description;

    private BigDecimal price;

    private Long quantity;
    private String idAvatar;
    private String fileName;
    private String fileFolder;
    private String fileUrl;


}
