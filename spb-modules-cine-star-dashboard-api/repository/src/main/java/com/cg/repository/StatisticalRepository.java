package com.cg.repository;

import com.cg.domain.entity.Statistical;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatisticalRepository extends JpaRepository<Statistical, Long> {
}
