package com.cg.repository;

import com.cg.domain.entity.BillTicketItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BillTicketItemRepository extends JpaRepository<BillTicketItem, Long> {
}
