class AppUtils {
    static DOMAIN_SERVER = location.origin;
    static CUSTOMER_API = this.DOMAIN_SERVER + "/api/customers";
    static STAFF_API = this.DOMAIN_SERVER + "/api/staffs";
    static ROLE_API = this.DOMAIN_SERVER + "/api/roles";
    static USER_API = this.DOMAIN_SERVER + "/api/users";
    static SHOWTIME_API = this.DOMAIN_SERVER + "/api/showtimes";
    static PROVINCE_URL = "https://vapi.vnappmob.com/api/province/";
    static MOVIE_API = this.DOMAIN_SERVER+"/api/movies";
    static THEATER_URL = this.DOMAIN_SERVER + "/api/theaters";
    static ROOM_URL = this.DOMAIN_SERVER + "/api/rooms";
    static FOOD_API = this.DOMAIN_SERVER + "/api/foods";

    static BASE_URL_CLOUD_IMAGE = "https://res.cloudinary.com/dirtzhdru/image/upload";
    static BASE_SCALE_IMAGE = "c_limit,w_250,h_100,q_100";
    static BASE_SCALE_IMAGE_SHOP = "c_limit,w_200,h_200,q_100";
    static BASE_SCALE_IMAGE_250_250 = "c_limit,w_250,h_250,q_100";
    static BASE_SCALE_IMAGE_MOVIE_350_350_100 = "c_limit,w_350,h_350,q_100";

    static SCALE_IMAGE_W100_H100_Q100 = "c_limit,w_100,h_100,q_100";


    static AlertMessageEn = class {
        static SUCCESS_CREATED = "Successful data generation !";
        static SUCCESS_UPDATED = "Data update successful !";
        static SUCCESS_DEACTIVATE = "Deactivate the customer successfully !";

        static ERROR_400 = "The operation failed, please check the data again.";
        static ERROR_401 = "Access Denied! Invalid credentials.";
        static ERROR_403 = "Access Denied! You are not authorized to perform this function.";
        static ERROR_404 = "This content has been removed or does not exist";
        static ERROR_500 = "Data saving failed, please contact the system administrator.";

        static ERROR_LOADING_PROVINCE = "Loading list of provinces - cities failed !";
        static ERROR_LOADING_DISTRICT = "Loading list of district - ward failed !"
        static ERROR_LOADING_WARD = "Loading list of wards - communes - towns failed !";
    }

    static AlertMessageVi = class {
        static SUCCESS_CREATED = "Tạo dữ liệu thành công !";
        static SUCCESS_UPDATED = "Cập nhật dữ liệu thành công !";
        static SUCCESS_DEACTIVATE = "Hủy kích hoạt khách hàng thành công !";

        static ERROR_400 = "Thao tác không thành công, vui lòng kiểm tra lại dữ liệu.";
        static ERROR_401 = "Quyền truy cập bị từ chối! Thông tin đăng nhập không hợp lệ.";
        static ERROR_403 = "Quyền truy cập bị từ chối! Bạn không được phép thực hiện chức năng này.";
        static ERROR_404 = "Nội dung này đã bị xóa hoặc không tồn tại";
        static ERROR_500 = "Lưu dữ liệu không thành công, vui lòng liên hệ với quản trị viên hệ thống.";

        static ERROR_LOADING_PROVINCE = "Tải danh sách tỉnh - thành phố không thành công !";
        static ERROR_LOADING_DISTRICT = "Tải danh sách quận - phường - huyện không thành công !";
        static ERROR_LOADING_WARD = "Tải danh sách phường - xã - thị trấn không thành công !";
    }

    static SweetAlert = class {
        static showDeactivateConfirmDialog() {
            return Swal.fire({
                icon: 'warning',
                text: 'Are you sure to deactivate the selected customer ?',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, please deactivate this client !',
                cancelButtonText: 'Cancel',
            })
        }

        static showSuccessAlert(t) {
            Swal.fire({
                icon: 'success',
                title: t,
                position: 'top-end',
                showConfirmButton: false,
                timer: 1500
            })
        }

        static showErrorAlert(t) {
            Swal.fire({
                icon: 'error',
                title: 'Warning',
                text: t,
            })
        }

        static showError401() {
            Swal.fire({
                icon: 'error',
                title: 'Access Denied',
                text: 'Invalid credentials!',
            })
        }

        static showError403() {
            Swal.fire({
                icon: 'error',
                title: 'Access Denied',
                text: 'You are not authorized to perform this function!',
            })
        }
        static showError500() {
            Swal.fire({
                icon: 'error',
                title: 'Access Denied',
                text: 'The server system is having problems or is not accessible.',
            })
        }
    }



    static IziToast = class {
        static showSuccessAlert(m) {
            iziToast.success({
                title: 'OK! ',
                position: 'topRight',
                timeout: 2500,
                message: m
            });
        }

        static showErrorAlert(m) {
            iziToast.error({
                title: 'Error',
                position: 'topRight',
                timeout: 2500,
                message: m
            });
        }
    }

    static Notify = class {
        static showSuccessAlert(m) {
            $.notify(m, "success");
        }

        static showErrorAlert(m) {
            $.notify(m, "error");
        }
    }

    static formatNumber() {
        $(".num-space").number(true, 0, ',', ' ')
        $(".num-point").number(true, 0, ',', '.');
        $(".num-comma").number(true, 0, ',', ',');
    }

    static formatNumberSpace(x) {
        if (x == null) {
            return x;
        }
        return x.toString().replace(/ /g, "").replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    }

    static removeFormatNumberSpace(x) {
        if (x == null) {
            return x;
        }
        return x.toString().replace(/ /g, "")
    }

    static formatTooltip() {
        $('[data-toggle="tooltip"]').tooltip();
    }
}


class Customer {
    constructor(id, fullName, phone ,dob , email , status) {
        this.id = id;
        this.fullName = fullName;
        this.dob= dob;
        this.email= email;
        this.phone = phone;
        this.status = status;
    }
}
class Role {
    constructor(id, code) {
        this.id = id;
        this.code = code;
    }
}

class CustomerAvatar {
    constructor(id, fileName, fileFolder, fileUrl, fileType, cloudId, ts, customer) {
        this.id = id;
        this.fileName = fileName;
        this.fileFolder = fileFolder;
        this.fileUrl = fileUrl;
        this.fileType = fileType;
        this.cloudId = cloudId;
        this.ts = ts;
        this.customer = customer;
    }
}

class User {
    constructor(id, username, password, role) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.role = role;
    }
}

class Food {
    constructor(id , fullName , description , price , combo , foodAvatar) {
        this.id = id ;
        this.fullName = fullName ;
        this.description = description;
        this.price = price ;
        this.combo = combo ;
        this.foodAvatar = foodAvatar ;
    }
}
class FoodAvatar {
    constructor(id, fileName, fileFolder, fileUrl, fileType, cloudId, ts) {
        this.id = id;
        this.fileName = fileName;
        this.fileFolder = fileFolder;
        this.fileUrl = fileUrl;
        this.fileType = fileType;
        this.cloudId = cloudId;
        this.ts = ts;
    }
}
class Staff {
    constructor(id, fullName, email, phone, dob, locationRegion) {
        this.id = id;
        this.fullName = fullName;
        this.email = email;
        this.phone = phone;
        this.dob = dob;
        this.locationRegion = locationRegion;

    }
}

class LocationRegion {
    constructor(id, provinceId, provinceName, districtId, districtName, wardId, wardName, address,staffAvatar) {
        this.id = id;
        this.provinceId = provinceId;
        this.provinceName = provinceName;
        this.districtId = districtId;
        this.districtName = districtName;
        this.wardId = wardId;
        this.wardName = wardName;
        this.address = address;
        this.staffAvatar=staffAvatar;
    }
}

class StaffAvatar {
    constructor(id, fileName, fileFolder, fileUrl, fileType, cloudId, width, height) {
        this.id = id;
        this.fileName = fileName;
        this.fileFolder = fileFolder;
        this.fileUrl = fileUrl;
        this.fileType = fileType;
        this.cloudId = cloudId;
        this.width = width;
        this.height = height;
    }
}

class ShowTime {
    constructor(id, movie, room, dayShowTime, frameTime, theater) {
        this.id = id;
        this.movie = movie;
        this.room = room;
        this.dayShowTime = dayShowTime;
        this.frameTime = frameTime;
        this.theater = theater;
    }
}

class FrameTime {
    constructor(id, startTime, endTime) {
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
    }
}

class Movie {
    constructor(id, nameMovie, description, duration, startDay, category, country) {
        this.id = id;
        this.nameMovie = nameMovie;
        this.description = description;
        this.duration = duration;
        this.startDay = startDay;
        this.category = category;
        this.country = country;
    }
}

class Category {
    constructor(id, categoryName) {
        this.id = id;
        this.categoryName = categoryName;
    }
}

class Country {
    constructor(id, countryName) {
        this.id = id;
        this.countryName = countryName;
    }
}

class MovieAvatar {
    constructor(id, fileName, fileFolder, fileUrl, fileType, cloudId, width, height) {
        this.id = id;
        this.fileName = fileName;
        this.fileFolder = fileFolder;
        this.fileUrl = fileUrl;
        this.fileType = fileType;
        this.cloudId = cloudId;
        this.width = width;
        this.height = height;
    }
}

