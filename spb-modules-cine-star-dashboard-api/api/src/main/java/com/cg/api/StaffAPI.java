package com.cg.api;

import com.cg.domain.dto.customer.CustomerDTO;
import com.cg.domain.dto.staff.StaffDTO;
import com.cg.domain.dto.staff.StaffRequestDTO;
import com.cg.domain.entity.Customer;
import com.cg.domain.entity.Staff;
import com.cg.exception.DataInputException;
import com.cg.exception.EmailExistsException;
import com.cg.service.staff.IStaffService;
import com.cg.utils.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/staffs")
public class StaffAPI {

    @Autowired
    private IStaffService staffService;
    @Autowired
    private AppUtils appUtils;

    @GetMapping
    public ResponseEntity<?> getAllStaffs() {
        List<StaffDTO> staffDTOS = staffService.findAllStaffResponseDTO();
        return new ResponseEntity<>(staffDTOS, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getStaffById(@PathVariable Long id) {
        StaffDTO staffDTO=staffService.findStaffResponseDTOById(id);
        if(staffDTO==null){
            throw new DataInputException("Thao tác không thành công, nhân viên không tồn tại");
        }
        return new ResponseEntity<>(staffDTO,HttpStatus.OK);
    }
    @PatchMapping("/{id}")
    public ResponseEntity<?> updateStaffById(@PathVariable Long id, @Valid StaffRequestDTO staffRequestDTO, BindingResult bindingResult) {

        if (bindingResult.hasFieldErrors()) {
            if(bindingResult.getFieldErrors().size() > 1){
                return appUtils.mapErrorToResponse(bindingResult);
            }
            if(bindingResult.getFieldErrors().size() == 1 && bindingResult.getFieldErrors("file").size() == 0){
                return appUtils.mapErrorToResponse(bindingResult);
            }
        }
        if (staffService.existsStaffByEmailAndIdNot(staffRequestDTO.getEmail(),id)) {
            throw new EmailExistsException("Email đã được sử dụng ,vui lòng hãy nhập lại!.");
        }
            Staff newStaff = staffService.update(staffRequestDTO);
        StaffDTO staffDTO=newStaff.toStaffDTO(newStaff);
        return new ResponseEntity<>(staffDTO,HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<?> createStaff(@Valid StaffRequestDTO staffRequestDTO, BindingResult bindingResult) {

        if (bindingResult.hasFieldErrors()) {
            if(bindingResult.getFieldErrors().size() > 1){
                return appUtils.mapErrorToResponse(bindingResult);
            }
            if(bindingResult.getFieldErrors().size() == 1 && bindingResult.getFieldErrors("file").size() == 0){
                return appUtils.mapErrorToResponse(bindingResult);
            }
        }
        if (staffService.existsStaffByEmail(staffRequestDTO.getEmail())) {
            throw new EmailExistsException("Email đã được sử dụng ,vui lòng hãy nhập lại!.");
        }
        Staff newStaff = staffService.create(staffRequestDTO);
        StaffDTO staffDTO = newStaff.toStaffDTO(newStaff);
        return new ResponseEntity<>(staffDTO,HttpStatus.OK);
    }

    @DeleteMapping("/delete/{staffId}")
    public ResponseEntity<?> delete(@PathVariable Long staffId) {

        Optional<Staff> staffOptional = staffService.findById(staffId);

        if (!staffOptional.isPresent()) {
            throw new DataInputException("Id nhân viên không hợp lệ.");
        }

        try {
            staffService.softDelete(staffId);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        } catch (Exception e) {
            throw new DataInputException("Vui lòng liên hệ Administrator.");
        }
    }

}
