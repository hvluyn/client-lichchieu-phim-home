package com.cg.service.trailer;

import com.cg.domain.entity.Trailer;
import com.cg.repository.TrailerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TrailerServiceImpl implements ITrailerService{

    @Autowired
    private TrailerRepository trailerRepository;

    @Override
    public List<Trailer> findAll() {
        return trailerRepository.findAll();
    }

    @Override
    public Trailer getById(Long id) {
        return trailerRepository.getById(id);
    }

    @Override
    public Optional<Trailer> findById(Long id) {
        return trailerRepository.findById(id);
    }

    @Override
    public Trailer save(Trailer trailer) {
        return trailerRepository.save(trailer);
    }

    @Override
    public void remove(Long id) {

    }
}
