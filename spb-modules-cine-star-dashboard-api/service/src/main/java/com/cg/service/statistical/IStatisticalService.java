package com.cg.service.statistical;

import com.cg.domain.entity.Statistical;
import com.cg.service.IGeneralService;

public interface IStatisticalService extends IGeneralService<Statistical> {

}
