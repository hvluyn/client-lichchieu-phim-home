package com.cg.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Accessors(chain = true)
public class Theater {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false,name = "cinema_name")
    private String name;

//    @OneToOne
//    @JoinColumn(name = "locationRegion_id",referencedColumnName = "id",nullable = false,unique = true)
//    private LocationRegion locationRegion;
//
//    @ManyToOne
//    @JoinColumn(name = "branch_id",referencedColumnName = "id",nullable = false)
//    private Branch branch;
}
