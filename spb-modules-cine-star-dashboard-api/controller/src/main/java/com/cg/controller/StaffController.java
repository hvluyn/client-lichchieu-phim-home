package com.cg.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/staff")
public class StaffController {
    @GetMapping
    public String showIndexPage() {
        return "cp/staff/list";
    }

    @GetMapping("/create")
    public String showcreatePage() {
        return "cp/staff/create";
    }
}
