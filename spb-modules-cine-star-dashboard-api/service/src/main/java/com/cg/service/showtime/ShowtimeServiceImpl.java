package com.cg.service.showtime;

import com.cg.domain.dto.movie.MovieAndShowtimeDTO;
import com.cg.domain.dto.showtimes.ShowTimeDTO;
import com.cg.domain.dto.showtimes.ShowTimeRequest;
import com.cg.domain.entity.*;
import com.cg.exception.DataInputException;
import com.cg.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.FieldError;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Transactional
public class ShowtimeServiceImpl implements IShowtimeService {

    @Autowired
    private ShowTimeRepository showTimeRepository;
    @Autowired
    private TheaterRepository theaterRepository;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private MovieAvatarRepository movieAvatarRepository;

    @Override
    public List<ShowTime> findAll() {
        return showTimeRepository.findAll();
    }

    @Override
    public ShowTime getById(Long id) {
        return showTimeRepository.getById(id);
    }

    @Override
    public Optional<ShowTime> findById(Long id) {
        return showTimeRepository.findById(id);
    }

    @Override
    public ShowTime save(ShowTime showTime) {
        return showTimeRepository.save(showTime);
    }

    @Override
    public void remove(Long id) {

    }

    @Override
    public List<ShowTimeDTO> findAllShowTimeResponseDTO() {
        return showTimeRepository.findAllShowTimeResponseDTO();
    }

    @Override
    public ShowTime create(ShowTimeRequest showTimeRequest) {
        ShowTime showTime = new ShowTime();
        Optional<Theater> theaterOptional = theaterRepository.findById(showTimeRequest.getTheaterId());
        if (!theaterOptional.isPresent()) {
            throw new DataInputException("Rạp không tồn tại, vui long kiểm tra lại");
        }
        Optional<Room> roomOptional = roomRepository.findById(showTimeRequest.getRoomId());
        if (!roomOptional.isPresent()) {
            throw new DataInputException("Rạp không tồn tại, vui long kiểm tra lại");
        }
        Optional<Movie> movieOptional = movieRepository.findById(showTimeRequest.getMovieId());
        if (!movieOptional.isPresent()) {
            throw new DataInputException("Rạp không tồn tại, vui long kiểm tra lại");
        }
        showTime.setTheater(theaterOptional.get());
        showTime.setRoom(roomOptional.get());
        showTime.setMovie(movieOptional.get());
        try {
            showTime.setDayShowTime(new SimpleDateFormat("yyyy-MM-dd").parse(showTimeRequest.getDayShow()));
        } catch (Exception e) {
            throw new DataInputException("Ngày tháng không đúng định dạng");
        }
        showTime.setFrameTime(showTimeRequest.getFrameTime());
        if(checkShowTimeSlotByShowSchedule(showTime)){
            throw new DataInputException("Khung giờ đã có phim khác chiếu, vui lòng chọn lại khung giờ khác");
        };
        showTimeRepository.save(showTime);
        return showTime;
    }
    @Override
    public ShowTime update(ShowTimeRequest showTimeRequest) {
        Optional<ShowTime> showTimeOptional = showTimeRepository.findById(showTimeRequest.getId());
        if(!showTimeOptional.isPresent()){
            throw new DataInputException("Suất chiếu không tồn tại");
        }
        ShowTime showTime = showTimeOptional.get();
        Optional<Theater> theaterOptional = theaterRepository.findById(showTimeRequest.getTheaterId());
        if (!theaterOptional.isPresent()) {
            throw new DataInputException("Rạp không tồn tại, vui lòng kiểm tra lại");
        }
        Optional<Room> roomOptional = roomRepository.findById(showTimeRequest.getRoomId());
        if (!roomOptional.isPresent()) {
            throw new DataInputException("Rạp không tồn tại, vui lòng kiểm tra lại");
        }
        Optional<Movie> movieOptional = movieRepository.findById(showTimeRequest.getMovieId());
        if (!movieOptional.isPresent()) {
            throw new DataInputException("Rạp không tồn tại, vui lòng kiểm tra lại");
        }
        showTime.setTheater(theaterOptional.get());
        showTime.setRoom(roomOptional.get());
        showTime.setMovie(movieOptional.get());
        try {
            showTime.setDayShowTime(new SimpleDateFormat("yyyy-MM-dd").parse(showTimeRequest.getDayShow()));
        } catch (Exception e) {
            throw new DataInputException("Ngày tháng không đúng định dạng");
        }
        showTime.setFrameTime(showTimeRequest.getFrameTime());
        if(checkShowTimeSlotByShowSchedule(showTime)){
            throw new DataInputException("Khung giờ đã có phim khác chiếu, vui lòng chọn lại khung giờ khác");
        };
        showTimeRepository.save(showTime);
        return showTime;
    }

    @Override
    public List<Theater> findTheaterByMovieId(Long id) {
        return showTimeRepository.findTheaterByMovieId(id);
    }

    @Override
    public List<Date> findShowDateByMovieIdAndTheaterId(Long movieId, Long theaterId) {
        return showTimeRepository.findShowDateByMovieIdAndTheaterId(movieId,theaterId);
    }

    @Override
    public List<ShowTime> findShowTimeByDayShowTimeAndRoom(Date date,Room room) {
        return showTimeRepository.findShowTimeByDayShowTimeAndRoom(date,room);
    }

    private Boolean checkShowTimeSlotByShowSchedule(ShowTime showTime){
        int movieStarTime = getMinute(showTime.getFrameTime());
        int movieEndTime = (int) (movieStarTime + showTime.getMovie().getDuration())+15;
        Boolean flag=false;
        if(getMinute(showTime.getFrameTime()) < getMinute("08:00") || getMinute(showTime.getFrameTime()) > getMinute("23:00")) {
            flag=true;
            return flag;
        }
        List<ShowTime> showTimeList=findShowTimeByDayShowTimeAndRoom(showTime.getDayShowTime(),showTime.getRoom());
        for(int i = 0; i < showTimeList.size() - 1 ; i++) {
            int movieStart = getMinute(showTimeList.get(i).getFrameTime());
            int movieEnd = (int) (movieStart + showTimeList.get(i).getMovie().getDuration())+15;
            if(showTimeList.size()>0){
                if(movieStarTime<=movieStart && movieEndTime >= movieStart ){
                    flag=true;
                    return flag;
                }
                if(movieStarTime>=movieStart && movieStarTime <= movieEnd ){
                    flag=true;
                    return flag;
                }
                if(movieEndTime>=movieEnd && movieStarTime <= movieEnd ){
                    flag=true;
                    return flag;
                }
                if(movieEndTime<=movieEnd && movieStarTime >= movieStart ){
                    flag=true;
                    return flag;
                }
            }
        }

        return flag;
    }

    public int getMinute(String showTimeSlot) {
        String hour = showTimeSlot.substring(0, 2);
        String minutes = showTimeSlot.substring(3);

        return Integer.parseInt(hour) * 60 + Integer.parseInt(minutes);
    }

    @Override
    public void softDelete(Long showTimeId) {
        showTimeRepository.softDelete(showTimeId);
    }

    @Override
    public List<ShowTime> findShowDateByMovieIdAndTheaterIdAndDateShow(Long movieId, Long theaterId, Date date) {
        return showTimeRepository.findShowDateByMovieIdAndTheaterIdAndDateShow(movieId,theaterId,date);
    }

    @Override
    public List<Movie> findAllMovieShowToDayAndNextDayByTheaterId(Long id, Date date, Date nextDate) {
        return showTimeRepository.findAllMovieShowToDayAndNextDayByTheaterId(id,date,nextDate);
    }

    public Date addDate(Date date,int days)  {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return  cal.getTime();
    }

    @Override
    public List<ShowTime> findAllShowTimeToDayAndNextDayByTheaterId(Long id, Date date, Date nextDate) {
        return showTimeRepository.findAllShowTimeToDayAndNextDayByTheaterId(id,date,nextDate);
    }

    @Override
    public List<MovieAndShowtimeDTO> toMovieAndShowTimeDTO(List<Movie> movies, List<ShowTime> showTimes) {

        showTimes.sort(Comparator.comparing(ShowTime::getFrameTime));

        List<MovieAndShowtimeDTO> movieAndShowtimeDTOList =new ArrayList<>();
        for (Movie movie:movies) {
            MovieAndShowtimeDTO movieAndShowtimeDTO=movie.toMovieAndShowtimeDTO();

            MovieAvatar movieAvatar=movieAvatarRepository.findByMovie(movie);

            movieAndShowtimeDTO.setIdAvatar(movieAvatar.getId());
            movieAndShowtimeDTO.setFileFolder(movieAvatar.getFileFolder());
            movieAndShowtimeDTO.setFileName(movieAvatar.getFileName());
            movieAndShowtimeDTO.setFileUrl(movieAvatar.getFileUrl());

            List<ShowTime> showTimeList = new ArrayList<>();

            for ( ShowTime showtime: showTimes ) {
                if(showtime.getMovie().getId()==movie.getId()){
                    showTimeList.add(showtime);
                }
            }
            movieAndShowtimeDTO.setShowTimes(showTimeList);

            movieAndShowtimeDTOList.add(movieAndShowtimeDTO);
        }

        return movieAndShowtimeDTOList;
    }
}
