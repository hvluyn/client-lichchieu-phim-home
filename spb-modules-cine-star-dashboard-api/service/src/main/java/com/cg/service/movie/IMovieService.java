package com.cg.service.movie;

import com.cg.domain.dto.food.FoodDTO;
import com.cg.domain.dto.movie.MovieDTO;
import com.cg.domain.dto.movie.MovieRequestDTO;
import com.cg.domain.entity.Movie;
import com.cg.domain.entity.MovieAvatar;
import com.cg.domain.entity.Staff;
import com.cg.service.IGeneralService;

import java.util.List;

public interface IMovieService extends IGeneralService<Movie> {
    List<MovieDTO> findAllMovieResponseDTO();

    Movie create(MovieRequestDTO movieRequestDTO);
    Boolean existsMovieByNameMovie(String name);
    MovieAvatar findByMovie(Movie movie);
    MovieDTO findMovieResponseDTO(Long id);
    Boolean existsMovieByNameMovieAndIdNot(String name, Long id);

    Movie update(MovieRequestDTO movieRequestDTO);
}
