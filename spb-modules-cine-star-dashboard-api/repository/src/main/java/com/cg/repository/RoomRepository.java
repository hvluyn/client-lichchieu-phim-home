package com.cg.repository;

import com.cg.domain.entity.Room;
import com.cg.domain.entity.Theater;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RoomRepository extends JpaRepository<Room, Long> {
    List<Room> findAllByTheater(Theater theater);
}
