package com.cg.service.show;

import com.cg.domain.entity.Show;
import com.cg.service.IGeneralService;

public interface IShowService extends IGeneralService<Show> {

}
