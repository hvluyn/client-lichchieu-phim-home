package com.cg.api;

import com.cg.domain.dto.movie.MovieAndShowtimeDTO;
import com.cg.domain.dto.showtimes.ShowTimeDTO;
import com.cg.domain.dto.showtimes.ShowTimeRequest;
import com.cg.domain.entity.Movie;
import com.cg.domain.entity.ShowTime;
import com.cg.domain.entity.Staff;
import com.cg.exception.DataInputException;
import com.cg.service.showtime.IShowtimeService;
import com.cg.utils.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/showtimes")
public class ShowTimeAPI {

    @Autowired
    private IShowtimeService showtimeService;
    @Autowired
    private AppUtils appUtils;

    @GetMapping
    public ResponseEntity<?> getAllShowtimes() {
        List<ShowTimeDTO> showTimeDTOS= showtimeService.findAllShowTimeResponseDTO();
        return new ResponseEntity<>(showTimeDTOS, HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getShowtimesbyId(@PathVariable Long id) {
        Optional<ShowTime> showTimeOptional= showtimeService.findById(id);
        if(!showTimeOptional.isPresent()){
            throw new DataInputException("Suất chiếu không tồn tại");
        }
        return new ResponseEntity<>(showTimeOptional.get(), HttpStatus.OK);
    }

    @GetMapping("/{movieid}/{theaterid}")
    public ResponseEntity<?> getDayShowtimesbyMovieIdAndTheaterId(@PathVariable Long movieid,@PathVariable Long theaterid) {
       List<Date> list =showtimeService.findShowDateByMovieIdAndTheaterId(movieid,theaterid);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping("/{movieid}/{theaterid}/{date}")
    public ResponseEntity<?> getDayShowtimesbyMovieIdAndTheaterId(@PathVariable Long movieid,@PathVariable Long theaterid,@PathVariable String date) throws ParseException {
        List<ShowTime> list =showtimeService.findShowDateByMovieIdAndTheaterIdAndDateShow(movieid,theaterid,new SimpleDateFormat("yyyy-MM-dd").parse(date));
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping("/movies/{theaterid}")
    public ResponseEntity<?> getAllMoviesShowTimeByTheaterId(@PathVariable Long theaterid) throws ParseException {
        Date date=new SimpleDateFormat("yyyy-MM-dd").parse(java.time.LocalDate.now().toString());
        Date nextDate=showtimeService.addDate(date,1);
        List<Movie> listMovies =showtimeService.findAllMovieShowToDayAndNextDayByTheaterId(theaterid,date,nextDate);
        List<ShowTime> showTimeList =showtimeService.findAllShowTimeToDayAndNextDayByTheaterId(theaterid,date,nextDate);
        List<MovieAndShowtimeDTO> movieAndShowtimeDTOList=showtimeService.toMovieAndShowTimeDTO(listMovies,showTimeList);
        return new ResponseEntity<>(movieAndShowtimeDTOList, HttpStatus.OK);
    }
    @PostMapping()
    public ResponseEntity<?> createShowTime(@Valid ShowTimeRequest showTimeRequest, BindingResult bindingResult) {

        if (bindingResult.hasFieldErrors()) {
                return appUtils.mapErrorToResponse(bindingResult);
        }

        ShowTime showTime =showtimeService.create(showTimeRequest);
        return new ResponseEntity<>(showTime,HttpStatus.OK);
    }
    @PatchMapping("/{id}")
    public ResponseEntity<?> updateShowTime(@Valid ShowTimeRequest showTimeRequest, BindingResult bindingResult) {

        if (bindingResult.hasFieldErrors()) {
            return appUtils.mapErrorToResponse(bindingResult);
        }

        ShowTime showTime = showtimeService.update(showTimeRequest);
        return new ResponseEntity<>(showTime,HttpStatus.OK);
    }

    @DeleteMapping("/delete/{showTimeId}")
    public ResponseEntity<?> delete(@PathVariable Long showTimeId) {

        Optional<ShowTime> showTimeOptional = showtimeService.findById(showTimeId);

        if (!showTimeOptional.isPresent()) {
            throw new DataInputException("Id lịch chiếu phim không hợp lệ.");
        }

        try {
            showtimeService.softDelete(showTimeId);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        } catch (Exception e) {
            throw new DataInputException("Vui lòng liên hệ Administrator.");
        }
    }
}
