package com.cg.service.chair;

import com.cg.domain.entity.Chair;
import com.cg.service.IGeneralService;

public interface IChairService extends IGeneralService<Chair> {

}
