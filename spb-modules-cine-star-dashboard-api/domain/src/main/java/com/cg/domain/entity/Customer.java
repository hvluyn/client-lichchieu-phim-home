package com.cg.domain.entity;

import com.cg.domain.dto.customer.CustomerDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Accessors(chain = true)
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String fullName;

    @Column(nullable = false,unique = true)
    private String email;

    private Date dob;
    private String phone;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    private Boolean status;

    public CustomerDTO toCustomerDTO(Customer customer) {
        return new CustomerDTO()
                .setId(id)
                .setFullName(fullName)
                .setEmail(email)
                .setPhone(phone)
                .setDob(dob)
                .setStatus(status)
                ;
    }

}
