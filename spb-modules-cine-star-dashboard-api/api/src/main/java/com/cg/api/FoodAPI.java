package com.cg.api;

import com.cg.domain.dto.food.FoodDTO;
import com.cg.domain.dto.food.FoodRequestDTO;
import com.cg.domain.dto.staff.StaffDTO;
import com.cg.domain.dto.staff.StaffRequestDTO;
import com.cg.domain.entity.Food;
import com.cg.domain.entity.FoodAvatar;
import com.cg.domain.entity.Staff;
import com.cg.exception.DataInputException;
import com.cg.exception.EmailExistsException;
import com.cg.service.food.IFoodService;
import com.cg.service.staff.IStaffService;
import com.cg.utils.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/foods")
public class FoodAPI {

    @Autowired
    private IFoodService foodService;
    @Autowired
    private AppUtils appUtils;

    @GetMapping
    public ResponseEntity<?> getAllFoods() {
        List<FoodDTO> foodDTOS = foodService.findAllFoodResponseDTO();
        return new ResponseEntity<>(foodDTOS, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<?> getFoodById(@PathVariable Long id) {
        FoodDTO foodDTO=foodService.findAllFoodResponseDTOById(id);
        if(foodDTO == null){
            throw new DataInputException("Thao tác không thành công, sản phẩm không tồn tại");
        }
        return new ResponseEntity<>(foodDTO,HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<?> createFood(@Valid FoodRequestDTO foodRequestDTO, BindingResult bindingResult) {

        if (bindingResult.hasFieldErrors()) {
            if(bindingResult.getFieldErrors().size()>1){
                return appUtils.mapErrorToResponse(bindingResult);
            }
            if(bindingResult.getFieldErrors().size()==1 && bindingResult.getFieldErrors("file").size()==0){
                return appUtils.mapErrorToResponse(bindingResult);
            }
        }
        if (foodService.existsFoodByName(foodRequestDTO.getName())) {
            throw new EmailExistsException("Tên đã được sử dụng ,vui lòng hãy nhập lại!.");
        }
        Food newFood = foodService.create(foodRequestDTO);

        return new ResponseEntity<>(newFood,HttpStatus.OK);
    }
    @PatchMapping()
    public ResponseEntity<?> updateFood(@Valid FoodRequestDTO foodRequestDTO, BindingResult bindingResult) {

        if (bindingResult.hasFieldErrors()) {
            if(bindingResult.getFieldErrors().size()>1){
                return appUtils.mapErrorToResponse(bindingResult);
            }
            if(bindingResult.getFieldErrors().size()==1 && bindingResult.getFieldErrors("file").size()==0){
                return appUtils.mapErrorToResponse(bindingResult);
            }
        }
//        if (foodService.existsFoodByName(foodRequestDTO.getName())) {
//            throw new EmailExistsException("Tên đã được sử dụng ,vui lòng hãy nhập lại!.");
//        }
        Food newFood = foodService.update(foodRequestDTO);
        FoodDTO foodDTO = newFood.toFoodDTO(newFood);
        List<FoodAvatar> foodAvatars= foodService.findByFood(newFood);
        FoodAvatar foodAvatar=foodAvatars.get(0);
        foodDTO.setFileFolder(foodAvatar.getFileFolder());
        foodDTO.setIdAvatar(foodAvatar.getId());
        foodDTO.setFileUrl(foodAvatar.getFileUrl());
        foodDTO.setFileName(foodAvatar.getFileName());
        return new ResponseEntity<>(foodDTO,HttpStatus.OK);
    }

    @DeleteMapping("/delete/{foodId}")
    public ResponseEntity<?> delete(@PathVariable Long foodId) {

        Optional<Food> productOptional = foodService.findById(foodId);

        if (!productOptional.isPresent()) {
            throw new DataInputException("ID sản phẩm không hợp lệ, vui lòng hãy nhập lại!");
        }

        try {
            foodService.softDelete(foodId);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        } catch (Exception e) {
            throw new DataInputException("Vui lòng liên hệ Administrator.");
        }
    }

}
