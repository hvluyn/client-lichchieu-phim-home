package com.cg.service.ticket;

import com.cg.domain.entity.Ticket;
import com.cg.service.IGeneralService;

public interface ITicketService extends IGeneralService<Ticket> {

}
