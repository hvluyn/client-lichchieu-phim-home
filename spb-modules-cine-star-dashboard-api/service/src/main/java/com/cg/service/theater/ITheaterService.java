package com.cg.service.theater;

import com.cg.domain.entity.Theater;
import com.cg.service.IGeneralService;

public interface ITheaterService extends IGeneralService<Theater> {

}
