package com.cg.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Accessors(chain = true)
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false,name = "roomname")
    private String name;

    @Column(nullable = false)
    private String screenFormat;

    @Column(nullable = false)
    private Long seatRows;

    @Column(nullable = false)
    private Long maxSeatOfRow;

    @ManyToOne
    @JoinColumn(name = "theater_id",referencedColumnName = "id",nullable = false)
    private Theater theater;

}
