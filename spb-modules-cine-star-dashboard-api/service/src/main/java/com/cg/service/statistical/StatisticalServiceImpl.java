package com.cg.service.statistical;

import com.cg.domain.entity.Statistical;
import com.cg.repository.StatisticalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class StatisticalServiceImpl implements IStatisticalService {

    @Autowired
    private StatisticalRepository statisticalRepository;

    @Override
    public List<Statistical> findAll() {
        return statisticalRepository.findAll();
    }

    @Override
    public Statistical getById(Long id) {
        return statisticalRepository.getById(id);
    }

    @Override
    public Optional<Statistical> findById(Long id) {
        return statisticalRepository.findById(id);
    }

    @Override
    public Statistical save(Statistical statistical) {
        return statisticalRepository.save(statistical);
    }

    @Override
    public void remove(Long id) {

    }
}
