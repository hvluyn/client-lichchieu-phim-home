package com.cg.domain.dto.food;

import com.cg.domain.dto.staff.StaffRequestDTO;
import com.cg.domain.entity.Food;
import com.cg.domain.entity.Staff;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
public class FoodRequestDTO {

    private Long id;

    private String name;

    private String description;

    private BigDecimal price;

    private MultipartFile file;

    public Food toFood(FoodRequestDTO foodRequestDTO) {
        return new Food()
                .setId(id)
                .setName(name)
                .setDescription(description)
                .setPrice(price);
    }
}
