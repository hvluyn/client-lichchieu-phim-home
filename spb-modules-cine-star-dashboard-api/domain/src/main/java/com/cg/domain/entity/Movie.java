package com.cg.domain.entity;

import com.cg.domain.dto.movie.MovieAndShowtimeDTO;
import com.cg.domain.dto.movie.MovieDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Accessors(chain = true)
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false,name = "movie_name",unique = true)
    private String nameMovie;

    private String description;

    @Column(nullable = false)
    private Long duration;

    private Date startDay;

    @ManyToOne
    @JoinColumn(name = "category_id",referencedColumnName = "id")
    private CategoryMovie categoryMovie;

    @ManyToOne
    @JoinColumn(name = "country_id",referencedColumnName = "id")
    private Country country;


    public MovieDTO toMovieDTO() {
        return new MovieDTO()
                .setCategoryMovie(categoryMovie)
                .setDescription(description)
                .setId(id)
                .setNameMovie(nameMovie)
                .setCountry(country)
                .setDuration(duration)
                .setStartDay(startDay);
    }

    public MovieAndShowtimeDTO toMovieAndShowtimeDTO() {
        return new MovieAndShowtimeDTO()
                .setCategoryMovie(categoryMovie)
                .setDescription(description)
                .setId(id)
                .setNameMovie(nameMovie)
                .setCountry(country)
                .setDuration(duration)
                .setStartDay(startDay);
    }
}
