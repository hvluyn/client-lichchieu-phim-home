package com.cg.service.seatType;

import com.cg.domain.entity.Room;
import com.cg.domain.entity.SeatType;
import com.cg.service.IGeneralService;

public interface ISeatTypeService extends IGeneralService<SeatType> {
}
