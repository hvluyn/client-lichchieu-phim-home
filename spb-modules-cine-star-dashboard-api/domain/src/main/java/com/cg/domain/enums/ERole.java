package com.cg.domain.enums;

public enum ERole {
    ROLE_ADMIN,
    ROLE_CASHIER,
    ROLE_STAFF,
    ROLE_CUSTOMER
}
