package com.cg.domain.dto.showtimes;

import com.cg.domain.entity.Movie;
import com.cg.domain.entity.Room;
import com.cg.domain.entity.Theater;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
public class ShowTimeDTO {

    private Long id;


    private Movie movie;


    private Room room;

    private Date dayShowTime;

    private String frameTime;


    private Theater theater;
}
