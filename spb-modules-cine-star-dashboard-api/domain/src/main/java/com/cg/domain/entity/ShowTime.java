package com.cg.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Accessors(chain = true)
public class ShowTime {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "movie_id",referencedColumnName = "id",nullable = false)
    private Movie movie;

    @ManyToOne
    @JoinColumn(name = "room_id",referencedColumnName = "id",nullable = false)
    private Room room;

    private String frameTime;

    private Date dayShowTime;

    @ManyToOne
    @JoinColumn(name = "theater_id",referencedColumnName = "id",nullable = false)
    private Theater theater;

}
