package com.cg.repository;


import com.cg.domain.entity.Food;
import com.cg.domain.entity.FoodAvatar;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface FoodAvatarRepository extends JpaRepository<FoodAvatar, Long> {
    List<FoodAvatar> findByFood(Food food);

}
