package com.cg.service.country;

import com.cg.domain.entity.Country;
import com.cg.service.IGeneralService;

public interface ICountryService extends IGeneralService<Country> {

}
