package com.cg.repository;

import com.cg.domain.entity.CategoryMovie;
import com.cg.domain.entity.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<Country, Long> {
    Country findById(Country countryId);
}
