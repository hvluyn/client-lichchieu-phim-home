package com.cg.service.movie;

import com.cg.cloudinary.CloudinaryUploadUtil;
import com.cg.domain.dto.food.FoodDTO;
import com.cg.domain.dto.movie.MovieDTO;
import com.cg.domain.dto.movie.MovieRequestDTO;
import com.cg.domain.dto.staff.StaffRequestDTO;
import com.cg.domain.entity.*;
import com.cg.exception.DataInputException;
import com.cg.repository.CategoryMovieRepository;
import com.cg.repository.CountryRepository;
import com.cg.repository.MovieAvatarRepository;
import com.cg.repository.MovieRepository;
import com.cg.service.upload.IUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional
public class MovieServiceImpl implements IMovieService {

    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private CloudinaryUploadUtil uploadUtil;

    @Autowired
    private IUploadService uploadService;

    @Autowired
    private CategoryMovieRepository categoryMovieRepository;
    @Autowired
    private MovieAvatarRepository movieAvatarRepository;

    @Override
    public List<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Movie getById(Long id) {
        return movieRepository.getById(id);
    }

    @Override
    public Optional<Movie> findById(Long id) {
        return movieRepository.findById(id);
    }

    @Override
    public Movie save(Movie movie) {
        return movieRepository.save(movie);
    }

    @Override
    public void remove(Long id) {

    }

    @Override
    public List<MovieDTO> findAllMovieResponseDTO() {
        return movieRepository.findAllMovieResponseDTO();
    }

    @Override
    public Movie create(MovieRequestDTO movieRequestDTO) {
        Movie movie = movieRequestDTO.toMovie();
        movie.setId(null);
        Optional<Country> country = countryRepository.findById(movieRequestDTO.getCountryId());
        if (!country.isPresent()) {
            throw new DataInputException("Quốc gia không tồn tại");
        }
        movie.setCountry(country.get());
        Optional<CategoryMovie> categoryMovie = categoryMovieRepository.findById(movieRequestDTO.getCategoryMovieId());
        if (!categoryMovie.isPresent()) {
            throw new DataInputException("Thể loại không tồn tại");
        }
        movie.setCategoryMovie(categoryMovie.get());

        movie=movieRepository.save(movie);

        String fileType = movieRequestDTO.getFile().getContentType();

        assert fileType != null;

        fileType = fileType.substring(0, 5);

        MovieAvatar movieAvatar = new MovieAvatar();
        movieAvatar.setId(null);
        movieAvatar.setFileType(fileType);
        movieAvatar.setMovie(movie);
        MovieAvatar newMovieAvatar = movieAvatarRepository.save(movieAvatar);
        uploadAndSaveMovieImage(movieRequestDTO, newMovieAvatar);
        return movie;
    }

    private void uploadAndSaveMovieImage(MovieRequestDTO movieRequestDTO, MovieAvatar movieAvatar) {
        try {
            Map uploadResult = uploadService.uploadImage(movieRequestDTO.getFile(), uploadUtil.buildImageUploadParams(movieAvatar.getId(), uploadUtil.MOVIE_IMAGE_UPLOAD_FOLDER, uploadUtil.ERROR_IMAGE_UPLOAD));
            String fileUrl = (String) uploadResult.get("secure_url");
            String fileFormat = (String) uploadResult.get("format");

            movieAvatar.setFileName(movieAvatar.getId() + "." + fileFormat);
            movieAvatar.setFileUrl(fileUrl);
            movieAvatar.setFileFolder(uploadUtil.MOVIE_IMAGE_UPLOAD_FOLDER);
            movieAvatar.setCloudId(movieAvatar.getFileFolder() + "/" + movieAvatar.getId());
            movieAvatarRepository.save(movieAvatar);

        } catch (IOException e) {
            e.printStackTrace();
            throw new DataInputException("Upload hình ảnh thất bại");
        }
    }

    @Override
    public Boolean existsMovieByNameMovie(String name) {
        return movieRepository.existsMovieByNameMovie(name);
    }

    @Override
    public MovieAvatar findByMovie(Movie movie) {
        return movieAvatarRepository.findByMovie(movie);
    }

    @Override
    public MovieDTO findMovieResponseDTO(Long id) {
        return movieRepository.findMovieResponseDTO(id);
    }

    @Override
    public Boolean existsMovieByNameMovieAndIdNot(String name, Long id) {
        return existsMovieByNameMovieAndIdNot(name,id);
    }

    @Override
    public Movie update(MovieRequestDTO movieRequestDTO) {
        Optional<Movie> movieOptional = findById(movieRequestDTO.getId());
        if (!movieOptional.isPresent()) {
            throw new DataInputException("Nhân viên không tồn tại");
        }
        Movie movie = movieOptional.get();
        movie.setNameMovie(movieRequestDTO.getNameMovie());
        movie.setDuration(movieRequestDTO.getDuration());
        try {
            movie.setStartDay(new SimpleDateFormat("yyyy-MM-dd").parse(movieRequestDTO.getStartDay()));
        } catch (Exception e) {
            throw new DataInputException("Dữ liệu không hợp lệ");
        }
        Optional<Country> country = countryRepository.findById(movieRequestDTO.getCountryId());
        if (!country.isPresent()) {
            throw new DataInputException("Quốc gia không tồn tại");
        }
        movie.setCountry(country.get());
        Optional<CategoryMovie> categoryMovie = categoryMovieRepository.findById(movieRequestDTO.getCategoryMovieId());
        if (!categoryMovie.isPresent()) {
            throw new DataInputException("Thể loại không tồn tại");
        }
        movie.setCategoryMovie(categoryMovie.get());
        movie.setDescription(movieRequestDTO.getDescription());

        movie=movieRepository.save(movie);
        if (movieRequestDTO.getFile() == null) {
            return movie;
        }
        String fileType = movieRequestDTO.getFile().getContentType();

        assert fileType != null;

        fileType = fileType.substring(0, 5);

        MovieAvatar movieAvatar = new MovieAvatar();
        movieAvatar.setId(null);
        movieAvatar.setFileType(fileType);
        movieAvatarRepository.delete(movieAvatarRepository.findByMovie(movie));
        movieAvatar.setMovie(movie);
        MovieAvatar newMovieAvatar = movieAvatarRepository.save(movieAvatar);
        uploadAndSaveMovieImage(movieRequestDTO, newMovieAvatar);
        return movie;
    }
}
