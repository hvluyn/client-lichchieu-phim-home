package com.cg.domain.entity;

import com.cg.domain.dto.staff.StaffDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Accessors(chain = true)
public class Staff extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String fullName;

    @Column(nullable = false,unique = true)
    private String email;

    private Date dob;
    private String phone;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "locationRegion_id",referencedColumnName = "id",nullable = false)
    private LocationRegion locationRegion;

    @ManyToOne
    @JoinColumn(name = "staffAvatar_id",referencedColumnName = "id")
    private StaffAvatar staffAvatar;

    public StaffDTO toStaffDTO(Staff staff) {
        return new StaffDTO()
                .setId(id)
                .setFullName(fullName)
                .setEmail(email)
                .setPhone(phone)
                .setDob(dob)
                .setLocationRegion(locationRegion)
                .setFileFolder(staff.getStaffAvatar().getFileFolder())
                .setFileUrl(staff.getStaffAvatar().getFileUrl())
                .setFileName(staff.getStaffAvatar().getFileName())
                ;
    }
}
