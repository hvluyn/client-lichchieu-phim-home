package com.cg.domain.dto.staff;

import com.cg.domain.entity.LocationRegion;
import com.cg.domain.entity.Staff;
import com.cg.exception.DataInputException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Column;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
public class StaffRequestDTO {
    private Long id;

    @NotEmpty(message = "Tên nhân viên không được để trống")
    @Size(min = 3, max = 30, message = "Độ dài tên nằm trong khoảng 8-35 ký tự!")
    private String fullName;

    @NotEmpty(message = "Tên nhân viên không được để trống")
    @Pattern(regexp = "^[\\w]+@([\\w-]+\\.)+[\\w-]{2,6}$", message = "Email không hợp lệ!")
    @Size(min = 8, max = 35, message = "Độ dài email nằm trong khoảng 8-35 ký tự!")
    private String email;


    private String dob;
    private String phone;

    private String provinceId;


    private String provinceName;


    private String districtId;


    private String districtName;


    private String wardId;


    private String wardName;
    public LocationRegion getLocation(){
        return new LocationRegion()
                .setId(null)
                .setProvinceId(provinceId)
                .setProvinceName(provinceName)
                .setDistrictId(districtId)
                .setDistrictName(districtName)
                .setWardId(wardId)
                .setWardName(wardName);
    }
    private MultipartFile file;

    public Staff toStaff(StaffRequestDTO staffRequestDTO) {
        try {
            return new Staff()
                    .setId(id)
                    .setFullName(fullName)
                    .setEmail(email)
                    .setPhone(phone)
                    .setDob(new SimpleDateFormat("yyyy-MM-dd").parse(dob))
                    .setLocationRegion(getLocation())
                    ;
        } catch (Exception e) {
            throw new DataInputException("Dữ liệu không hợp lệ");
        }
    }

}
