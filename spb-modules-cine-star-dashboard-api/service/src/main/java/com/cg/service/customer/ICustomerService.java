package com.cg.service.customer;

import com.cg.domain.dto.customer.CustomerDTO;
import com.cg.domain.entity.Customer;
import com.cg.service.IGeneralService;

import java.util.List;

public interface ICustomerService extends IGeneralService<Customer> {
    List<CustomerDTO> findAllCustomerResponseDTO();
    CustomerDTO findCustomerResponseDTOById(Long id);
}
