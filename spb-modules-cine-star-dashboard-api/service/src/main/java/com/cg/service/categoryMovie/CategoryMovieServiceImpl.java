package com.cg.service.categoryMovie;

import com.cg.domain.entity.CategoryMovie;
import com.cg.repository.CategoryMovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CategoryMovieServiceImpl implements ICategoryMovieService {

    @Autowired
    private CategoryMovieRepository categoryMovieRepository;

    @Override
    public List<CategoryMovie> findAll() {
        return categoryMovieRepository.findAll();
    }

    @Override
    public CategoryMovie getById(Long id) {
        return categoryMovieRepository.getById(id);
    }

    @Override
    public Optional<CategoryMovie> findById(Long id) {
        return categoryMovieRepository.findById(id);
    }



    @Override
    public CategoryMovie save(CategoryMovie categoryMovie) {
        return categoryMovieRepository.save(categoryMovie);
    }

    @Override

   public void remove(Long id) {

    }
}
