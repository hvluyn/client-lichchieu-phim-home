package com.cg.repository;

import com.cg.domain.dto.customer.CustomerDTO;
import com.cg.domain.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    @Query("SELECT NEW com.cg.domain.dto.customer.CustomerDTO (" +
            "c.id, " +
            "c.fullName, " +
            "c.email, " +
            "c.dob, " +
            "c.phone, " +
            "c.status, " +
            "ca.fileName, " +
            "ca.fileFolder, " +
            "ca.fileUrl " +
            ") " +
            "FROM Customer AS c " +
            "JOIN CustomerAvatar AS ca " +
            "ON ca.customer = c "
    )
    List<CustomerDTO> findAllCustomerResponseDTO();
    @Query("SELECT NEW com.cg.domain.dto.customer.CustomerDTO (" +
            "c.id, " +
            "c.fullName, " +
            "c.email, " +
            "c.dob, " +
            "c.phone, " +
            "c.status, " +
            "ca.fileName, " +
            "ca.fileFolder, " +
            "ca.fileUrl " +
            ") " +
            "FROM Customer AS c " +
            "JOIN CustomerAvatar AS ca " +
            "ON ca.customer = c where c.id=?1"
    )
    CustomerDTO findCustomerResponseDTOById(Long id);
}
