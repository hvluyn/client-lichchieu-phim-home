package com.cg.service.billTicketItem;

import com.cg.domain.entity.BillTicketItem;
import com.cg.service.IGeneralService;

public interface IBillTicketItemService extends IGeneralService<BillTicketItem> {

}
