package com.cg.repository;

import com.cg.domain.entity.CartFoodItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartFoodItemRepository extends JpaRepository<CartFoodItem, Long> {
}
