package com.cg.controller;

import com.cg.domain.dto.movie.MovieDTO;
import com.cg.service.categoryMovie.ICategoryMovieService;
import com.cg.service.country.ICountryService;
import com.cg.service.movie.IMovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/movie")
public class MovieController {
    @Autowired
    private IMovieService movieService;
    @GetMapping
    public String showIndexPage() {
        return "cp/movie/list";
    }

    @GetMapping("/create")
    public String showCreatePage() {
        return "cp/movie/create";
    }

    @GetMapping("/view/{id}")
    public String showViewPage(@PathVariable Long id, Model model) {
        MovieDTO movieDTO=movieService.findMovieResponseDTO(id);
        model.addAttribute("movie",movieDTO);
        return "cp/movie/view";
    }
}
