package com.cg.service.showtime;

import com.cg.domain.dto.movie.MovieAndShowtimeDTO;
import com.cg.domain.dto.showtimes.ShowTimeDTO;
import com.cg.domain.dto.showtimes.ShowTimeRequest;
import com.cg.domain.entity.Movie;
import com.cg.domain.entity.Room;
import com.cg.domain.entity.ShowTime;
import com.cg.domain.entity.Theater;
import com.cg.service.IGeneralService;

import java.util.Date;
import java.util.List;

public interface IShowtimeService extends IGeneralService<ShowTime> {

    List<ShowTimeDTO> findAllShowTimeResponseDTO();

    ShowTime create(ShowTimeRequest showTimeRequest);
    List<ShowTime> findShowTimeByDayShowTimeAndRoom(Date date, Room room);
    ShowTime update(ShowTimeRequest showTimeRequest);

    List<Theater> findTheaterByMovieId(Long id);
    List<Date> findShowDateByMovieIdAndTheaterId(Long movieId,Long theaterId);
    public void softDelete(Long showTimeId);
    List<ShowTime> findShowDateByMovieIdAndTheaterIdAndDateShow(Long movieId,Long theaterId, Date date);

    List<Movie> findAllMovieShowToDayAndNextDayByTheaterId(Long id, Date date, Date nextDate);
    public Date addDate(Date date,int days);

    List<ShowTime> findAllShowTimeToDayAndNextDayByTheaterId(Long id, Date date, Date nextDate);

    List<MovieAndShowtimeDTO> toMovieAndShowTimeDTO(List<Movie> movies, List<ShowTime> showTimes);


}
