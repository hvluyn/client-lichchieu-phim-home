package com.cg.repository;

import com.cg.domain.dto.food.FoodDTO;
import com.cg.domain.dto.movie.MovieDTO;
import com.cg.domain.entity.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MovieRepository extends JpaRepository<Movie, Long> {
    @Query("SELECT NEW com.cg.domain.dto.movie.MovieDTO (" +
            "f.id, " +
            "f.nameMovie, " +
            "f.description, " +
            "f.duration, " +
            "f.startDay, " +
            "f.categoryMovie, " +
            "f.country, " +
            "fa.id,"+
            "fa.fileName, " +
            "fa.fileFolder, " +
            "fa.fileUrl " +
            ") " +
            "FROM Movie AS f " +
            "JOIN MovieAvatar AS fa " +
            "ON f = fa.movie "
    )
    List<MovieDTO> findAllMovieResponseDTO();
    @Query("SELECT NEW com.cg.domain.dto.movie.MovieDTO (" +
            "f.id, " +
            "f.nameMovie, " +
            "f.description, " +
            "f.duration, " +
            "f.startDay, " +
            "f.categoryMovie, " +
            "f.country, " +
            "fa.id,"+
            "fa.fileName, " +
            "fa.fileFolder, " +
            "fa.fileUrl " +
            ") " +
            "FROM Movie AS f " +
            "JOIN MovieAvatar AS fa " +
            "ON f = fa.movie where f.id=?1"
    )
    MovieDTO findMovieResponseDTO(Long id);
    Boolean existsMovieByNameMovie(String name);
    Boolean existsMovieByNameMovieAndIdNot(String name, Long id);
}
