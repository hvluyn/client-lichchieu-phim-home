package com.cg.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/showtimes")
public class ShowTimeController {
    @GetMapping
    public String showIndexPage() {
        return "cp/showtime/list";
    }
}
