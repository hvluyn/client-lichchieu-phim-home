package com.cg.service.role;

import com.cg.domain.entity.Role;
import com.cg.service.IGeneralService;


public interface IRoleService extends IGeneralService<Role> {

}

