package com.cg.repository;

import com.cg.domain.entity.CategoryMovie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryMovieRepository extends JpaRepository<CategoryMovie, Long> {

}
