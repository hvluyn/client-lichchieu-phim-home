package com.cg.service.staff;

import com.cg.domain.dto.customer.CustomerDTO;
import com.cg.domain.dto.staff.StaffDTO;
import com.cg.domain.dto.staff.StaffRequestDTO;
import com.cg.domain.entity.Staff;
import com.cg.service.IGeneralService;

import java.util.List;

public interface IStaffService extends IGeneralService<Staff> {
    List<StaffDTO> findAllStaffResponseDTO();
    StaffDTO findStaffResponseDTOById(Long id);
    Boolean existsStaffByEmailAndIdNot(String email,Long id);
    Boolean existsStaffByEmail(String email);
    Staff update(StaffRequestDTO staffRequestDTO);
    Staff create(StaffRequestDTO staffRequestDTO);

    public void softDelete(Long staffId);

}
