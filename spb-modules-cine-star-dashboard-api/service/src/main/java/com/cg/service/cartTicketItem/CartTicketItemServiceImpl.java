package com.cg.service.cartTicketItem;

import com.cg.domain.entity.CartTicketItem;
import com.cg.repository.CartTicketItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CartTicketItemServiceImpl implements ICartTicketItemService {

    @Autowired
    private CartTicketItemRepository cartTicketItemRepository;

    @Override
    public List<CartTicketItem> findAll() {
        return cartTicketItemRepository.findAll();
    }

    @Override
    public CartTicketItem getById(Long id) {
        return cartTicketItemRepository.getById(id);
    }

    @Override
    public Optional<CartTicketItem> findById(Long id) {
        return cartTicketItemRepository.findById(id);
    }



    @Override
    public CartTicketItem save(CartTicketItem cartTicketItem) {
        return cartTicketItemRepository.save(cartTicketItem);
    }

    @Override

   public void remove(Long id) {

    }
}
