package com.cg.service.trailer;

import com.cg.domain.entity.Trailer;
import com.cg.service.IGeneralService;

public interface ITrailerService extends IGeneralService<Trailer> {

}
