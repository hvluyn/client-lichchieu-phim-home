package com.cg.api;

import com.cg.domain.dto.movie.MovieDTO;
import com.cg.domain.dto.movie.MovieRequestDTO;
import com.cg.domain.dto.staff.StaffDTO;
import com.cg.domain.dto.staff.StaffRequestDTO;
import com.cg.domain.entity.*;
import com.cg.exception.DataInputException;
import com.cg.exception.EmailExistsException;
import com.cg.repository.CountryRepository;
import com.cg.service.categoryMovie.ICategoryMovieService;
import com.cg.service.country.ICountryService;
import com.cg.service.movie.IMovieService;
import com.cg.service.staff.IStaffService;
import com.cg.utils.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/movies")
public class MovieAPI {
    @Autowired
    private IMovieService movieService;
    @Autowired
    private ICountryService countryService;
    @Autowired
    private ICategoryMovieService categoryMovieService;
    @Autowired
    private AppUtils appUtils;
    @Autowired
    private CountryRepository countryRepository;

    @GetMapping
    public ResponseEntity<?> getAllMovie() {
        List<MovieDTO> movieDTOS= movieService.findAllMovieResponseDTO();
        return new ResponseEntity<>(movieDTOS, HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getMovieById(@PathVariable Long id) {
        MovieDTO movieDTO=movieService.findMovieResponseDTO(id);
        if(movieDTO==null){
            throw new DataInputException("Thao tác không thành công, bộ phim không tồn tại");
        }
        return new ResponseEntity<>(movieDTO,HttpStatus.OK);
    }
    @GetMapping("/country")
    public ResponseEntity<?> getAllMovieCountry() {
        List<Country> countries= countryService.findAll();
        return new ResponseEntity<>(countries, HttpStatus.OK);
    }
    @GetMapping("/category")
    public ResponseEntity<?> getAllMovieCategory() {
        List<CategoryMovie> categoryMovies= categoryMovieService.findAll();
        return new ResponseEntity<>(categoryMovies, HttpStatus.OK);
    }
    @PostMapping()
    public ResponseEntity<?> createMovie(@Valid MovieRequestDTO movieRequestDTO, BindingResult bindingResult) {

        if (bindingResult.hasFieldErrors()) {
            if(bindingResult.getFieldErrors().size()>1){
                return appUtils.mapErrorToResponse(bindingResult);
            }
            if(bindingResult.getFieldErrors().size()==1 && bindingResult.getFieldErrors("file").size()==0){
                return appUtils.mapErrorToResponse(bindingResult);
            }
        }
        if (movieService.existsMovieByNameMovie(movieRequestDTO.getNameMovie())) {
            throw new EmailExistsException("Tên phim đã được tồn tại ,vui lòng hãy nhập lại!.");
        }
        Movie movie =movieService.create(movieRequestDTO);
        MovieDTO movieDTO=movie.toMovieDTO();
        MovieAvatar movieAvatar=movieService.findByMovie(movie);
        movieDTO.setIdAvatar(movieAvatar.getId());
        movieDTO.setFileFolder(movieAvatar.getFileFolder());
        movieDTO.setFileUrl(movieAvatar.getFileUrl());
        movieDTO.setFileName(movieAvatar.getFileName());
        return new ResponseEntity<>(movieDTO,HttpStatus.OK);
    }
    @PatchMapping("/{id}")
    public ResponseEntity<?> updateMovieById(@PathVariable Long id, @Valid MovieRequestDTO movieRequestDTO, BindingResult bindingResult) {

        if (bindingResult.hasFieldErrors()) {
            if(bindingResult.getFieldErrors().size()>1){
                return appUtils.mapErrorToResponse(bindingResult);
            }
            if(bindingResult.getFieldErrors().size()==1 && bindingResult.getFieldErrors("file").size()==0){
                return appUtils.mapErrorToResponse(bindingResult);
            }
        }
//        if (movieService.existsMovieByNameMovieAndIdNot(movieRequestDTO.getNameMovie(),id)) {
//            throw new EmailExistsException("Tên phim đã được sử dụng ,vui lòng hãy nhập lại!.");
//        }
        Movie movie =movieService.update(movieRequestDTO);
        MovieDTO movieDTO=movie.toMovieDTO();
        MovieAvatar movieAvatar=movieService.findByMovie(movie);
        movieDTO.setIdAvatar(movieAvatar.getId());
        movieDTO.setFileFolder(movieAvatar.getFileFolder());
        movieDTO.setFileUrl(movieAvatar.getFileUrl());
        movieDTO.setFileName(movieAvatar.getFileName());
        return new ResponseEntity<>(movieDTO,HttpStatus.OK);
    }

}
