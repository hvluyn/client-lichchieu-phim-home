package com.cg.repository;

import com.cg.domain.dto.showtimes.ShowTimeDTO;
import com.cg.domain.entity.Movie;
import com.cg.domain.entity.Room;
import com.cg.domain.entity.ShowTime;
import com.cg.domain.entity.Theater;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface ShowTimeRepository extends JpaRepository<ShowTime, Long> {
    @Query("SELECT NEW com.cg.domain.dto.showtimes.ShowTimeDTO (" +
            "s.id, " +
            "s.movie, " +
            "s.room, " +
            "s.dayShowTime, " +
            "s.frameTime, " +
            "s.theater) " +
            "FROM ShowTime AS s "
    )
    List<ShowTimeDTO> findAllShowTimeResponseDTO();

    List<ShowTime> findShowTimeByDayShowTimeAndRoom(Date date, Room room);

    @Query("SELECT DISTINCT st.theater " +
            "FROM ShowTime AS st " +
            "WHERE st.movie.id = ?1 "
    )
    List<Theater> findTheaterByMovieId(Long id);

    @Query("SELECT DISTINCT st.dayShowTime " +
            "FROM ShowTime AS st " +
            "WHERE st.movie.id = ?1 AND st.theater.id=?2"
    )
    List<Date> findShowDateByMovieIdAndTheaterId(Long movieId,Long theaterId);

    @Query("SELECT DISTINCT st.movie " +
            "FROM ShowTime AS st " +
            "WHERE st.theater.id = ?1 AND ( st.dayShowTime = ?2 OR st.dayShowTime = ?3 )"
    )
    List<Movie> findAllMovieShowToDayAndNextDayByTheaterId(Long id, Date date, Date nextDate);

    @Query("SELECT st " +
            "FROM ShowTime AS st " +
            "WHERE st.theater.id = ?1 AND ( st.dayShowTime = ?2 OR st.dayShowTime = ?3 )"
    )
    List<ShowTime> findAllShowTimeToDayAndNextDayByTheaterId(Long id, Date date, Date nextDate);

    @Query("SELECT st " +
            "FROM ShowTime AS st " +
            "WHERE st.movie.id = ?1 AND st.theater.id=?2 AND st.dayShowTime=?3"
    )
    List<ShowTime> findShowDateByMovieIdAndTheaterIdAndDateShow(Long movieId,Long theaterId, Date date);

    @Modifying
    @Query("DELETE FROM ShowTime AS st WHERE st.id = :showTimeId")
    void softDelete(@Param("showTimeId") long showTimeId);
}
