package com.cg.service.cart;

import com.cg.domain.entity.Cart;
import com.cg.service.IGeneralService;

public interface ICartService extends IGeneralService<Cart> {

}
