package com.cg.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Accessors(chain = true)
public class Chair {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    private String name;

    private String price;

    @Column(name = "seat_form")
    private String seatForm;

    @Column(name = "seat_row_name")
    private String seatRowName;

    @Column(name = "seat_row_position")
    private Long seatRowPosition;

    @Column(name = "left_position")
    private Double leftPosition;

    @Column(name = "seat_position")
    private Double topPosition;

    @ManyToOne
    @JoinColumn(name = "seat_type_id",referencedColumnName = "id",nullable = false)
    private SeatType seatType;

    @ManyToOne
    @JoinColumn(name = "roomid",referencedColumnName = "id",nullable = false)
    private Room room;

    @ManyToOne
    @JoinColumn(name = "theater_id",referencedColumnName = "id",nullable = false)
    private Theater theater;

}
