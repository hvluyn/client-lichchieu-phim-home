package com.cg.service.movieavatar;

import com.cg.domain.entity.MovieAvatar;
import com.cg.repository.MovieAvatarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class MovieAvatarServiceImpl implements IMovieAvatarService {

    @Autowired
    private MovieAvatarRepository movieAvatarRepository;

    @Override
    public List<MovieAvatar> findAll() {
        return movieAvatarRepository.findAll();
    }

    @Override
    public MovieAvatar getById(Long id) {
        return movieAvatarRepository.getById(id);
    }

    @Override
    public Optional<MovieAvatar> findById(Long id) {
        return movieAvatarRepository.findById(id);
    }

    @Override
    public MovieAvatar save(MovieAvatar movieAvatar) {
        return movieAvatarRepository.save(movieAvatar);
    }

    @Override
    public void remove(Long id) {

    }
}
