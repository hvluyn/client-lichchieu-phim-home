package com.cg.domain.dto.movie;

import com.cg.domain.entity.CategoryMovie;
import com.cg.domain.entity.Country;
import com.cg.domain.entity.Movie;
import com.cg.exception.DataInputException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.web.multipart.MultipartFile;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
public class MovieRequestDTO {
    private Long id;

    private String nameMovie;

    private String description;


    private Long duration;

    private String startDay;


    private Long categoryMovieId;


    private Long countryId;

    private MultipartFile file;

    public Movie toMovie() {
        try {
            return new Movie()
                    .setId(id)
                    .setNameMovie(nameMovie)
                    .setDescription(description)
                    .setStartDay(new SimpleDateFormat("yyyy-MM-dd").parse(startDay))
                    .setDuration(duration);
        } catch (Exception e) {
            throw new DataInputException("Định dạng ngày tháng không hợp lệ");
        }
    }
}
