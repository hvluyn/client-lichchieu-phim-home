package com.cg.service.food;

import com.cg.domain.dto.food.FoodDTO;
import com.cg.domain.dto.food.FoodRequestDTO;
import com.cg.domain.dto.staff.StaffRequestDTO;
import com.cg.domain.entity.Food;
import com.cg.domain.entity.FoodAvatar;
import com.cg.service.IGeneralService;

import java.util.List;

public interface IFoodService extends IGeneralService<Food> {
    List<FoodDTO> findAllFoodResponseDTO();
    FoodDTO findAllFoodResponseDTOById(Long id);
    Boolean existsFoodByName(String name);

    Food create(FoodRequestDTO foodRequestDTO);
    public Food update(FoodRequestDTO foodRequestDTO);
    Food findByName(String name);

    List<FoodAvatar> findByFood(Food food);

    void softDelete(Long foodId);
}
