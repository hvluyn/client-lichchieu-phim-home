package com.cg.service.foodAvatar;

import com.cg.domain.entity.FoodAvatar;
import com.cg.repository.FoodAvatarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class FoodAvatarServiceImpl implements IFoodAvatarService {

    @Autowired
    private FoodAvatarRepository foodAvatarRepository;

    @Override
    public List<FoodAvatar> findAll() {
        return foodAvatarRepository.findAll();
    }

    @Override
    public FoodAvatar getById(Long id) {
        return foodAvatarRepository.getById(id);
    }

    @Override
    public Optional<FoodAvatar> findById(Long id) {
        return foodAvatarRepository.findById(id);
    }



    @Override
    public FoodAvatar save(FoodAvatar foodAvatar) {
        return foodAvatarRepository.save(foodAvatar);
    }

    @Override

   public void remove(Long id) {

    }
}
