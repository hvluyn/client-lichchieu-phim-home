package com.cg.service.billFoodItem;

import com.cg.domain.entity.Bill;
import com.cg.domain.entity.BillFoodItem;
import com.cg.service.IGeneralService;

public interface IBillFoodItemService extends IGeneralService<BillFoodItem> {

}
