package com.cg.service.bill;

import com.cg.domain.entity.Bill;
import com.cg.service.IGeneralService;

public interface IBillService extends IGeneralService<Bill> {

}
