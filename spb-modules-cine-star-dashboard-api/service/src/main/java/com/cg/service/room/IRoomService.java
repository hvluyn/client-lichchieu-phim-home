package com.cg.service.room;

import com.cg.domain.entity.Room;
import com.cg.domain.entity.Theater;
import com.cg.service.IGeneralService;

import java.util.List;

public interface IRoomService extends IGeneralService<Room> {
    List<Room> findAllByTheater(Theater theater);
}
