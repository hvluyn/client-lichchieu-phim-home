package com.cg.repository;

import com.cg.domain.entity.BillFoodItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BillFoodItemRepository extends JpaRepository<BillFoodItem, Long> {
}
