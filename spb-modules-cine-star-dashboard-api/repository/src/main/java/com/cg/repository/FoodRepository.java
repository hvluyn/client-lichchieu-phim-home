package com.cg.repository;

import com.cg.domain.dto.food.FoodDTO;
import com.cg.domain.dto.staff.StaffDTO;
import com.cg.domain.entity.Food;
import com.cg.domain.entity.FoodAvatar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FoodRepository extends JpaRepository<Food, Long> {
    @Query("SELECT NEW com.cg.domain.dto.food.FoodDTO (" +
            "f.id, " +
            "f.name, " +
            "f.description, " +
            "f.price, " +
            "f.quantity, " +
            "fa.id,"+
            "fa.fileName, " +
            "fa.fileFolder, " +
            "fa.fileUrl " +
            ") " +
            "FROM Food AS f " +
            "JOIN FoodAvatar AS fa " +
            "ON f = fa.food " +
            "WHERE f.deleted = false "
    )
    List<FoodDTO> findAllFoodResponseDTO();

    @Query("SELECT NEW com.cg.domain.dto.food.FoodDTO (" +
            "f.id, " +
            "f.name, " +
            "f.description, " +
            "f.price, " +
            "f.quantity, " +
            "fa.id,"+
            "fa.fileName, " +
            "fa.fileFolder, " +
            "fa.fileUrl " +
            ") " +
            "FROM Food AS f " +
            "JOIN FoodAvatar AS fa " +
            "ON f = fa.food WHERE f.id = ?1 "
    )
    FoodDTO findAllFoodResponseDTOById(Long id);
    Food findByName(String name);
    Boolean existsFoodByName(String name);

    @Modifying
    @Query("UPDATE Food AS f SET f.deleted = true WHERE f.id = :foodId")
    void softDelete(@Param("foodId") long foodId);

}
