package com.cg.repository;

import com.cg.domain.entity.Movie;
import com.cg.domain.entity.MovieAvatar;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieAvatarRepository extends JpaRepository<MovieAvatar, Long> {
    MovieAvatar findByMovie(Movie movie);
}
