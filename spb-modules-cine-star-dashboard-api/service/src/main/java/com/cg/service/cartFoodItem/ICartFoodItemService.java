package com.cg.service.cartFoodItem;

import com.cg.domain.entity.CartFoodItem;
import com.cg.service.IGeneralService;

public interface ICartFoodItemService extends IGeneralService<CartFoodItem> {

}
