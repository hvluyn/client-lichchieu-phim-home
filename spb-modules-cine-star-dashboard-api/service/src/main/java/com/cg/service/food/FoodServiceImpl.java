package com.cg.service.food;

import com.cg.cloudinary.CloudinaryUploadUtil;
import com.cg.domain.dto.food.FoodDTO;
import com.cg.domain.dto.food.FoodRequestDTO;
import com.cg.domain.dto.staff.StaffRequestDTO;
import com.cg.domain.entity.Food;
import com.cg.domain.entity.FoodAvatar;
import com.cg.domain.entity.Staff;
import com.cg.domain.entity.StaffAvatar;
import com.cg.exception.DataInputException;
import com.cg.repository.FoodAvatarRepository;
import com.cg.repository.FoodRepository;
import com.cg.service.upload.IUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional
public class FoodServiceImpl implements IFoodService {

    @Autowired
    private FoodRepository foodRepository;
    @Autowired
    private FoodAvatarRepository foodAvatarRepository;
    @Autowired
    private CloudinaryUploadUtil uploadUtil;

    @Autowired
    private IUploadService uploadService;

    @Override
    public List<Food> findAll() {
        return foodRepository.findAll();
    }

    @Override
    public Food getById(Long id) {
        return foodRepository.getById(id);
    }

    @Override
    public Optional<Food> findById(Long id) {
        return foodRepository.findById(id);
    }



    @Override
    public Food save(Food food) {
        return foodRepository.save(food);
    }

    @Override

   public void remove(Long id) {

    }

    @Override
    public List<FoodDTO> findAllFoodResponseDTO() {
        return foodRepository.findAllFoodResponseDTO();
    }

    @Override
    public FoodDTO findAllFoodResponseDTOById(Long id) {
        return foodRepository.findAllFoodResponseDTOById(id);
    }

    @Override
    public Boolean existsFoodByName(String name) {
        return foodRepository.existsFoodByName(name);
    }

    @Override
    public Food create(FoodRequestDTO foodRequestDTO) {
        Food food = foodRequestDTO.toFood(foodRequestDTO);
        food.setId(null);
        food=foodRepository.save(food);

        String fileType = foodRequestDTO.getFile().getContentType();

        assert fileType != null;

        fileType = fileType.substring(0, 5);

        FoodAvatar foodAvatar = new FoodAvatar();
        foodAvatar.setId(null);
        foodAvatar.setFileType(fileType);
        foodAvatar=foodAvatarRepository.save(foodAvatar);
        uploadAndSaveFoodImage(foodRequestDTO, foodAvatar);
        foodAvatar.setFood(food);
        return food;

    }
    @Override
    public Food update(FoodRequestDTO foodRequestDTO) {
        Optional<Food> foodOptional = findById(foodRequestDTO.getId());
        if (!foodOptional.isPresent()) {
            throw new DataInputException("Sản phẩm không tồn tại");
        }
        Food food = foodOptional.get();
        food.setName(foodRequestDTO.getName());
        food.setPrice(foodRequestDTO.getPrice());
        food.setDescription(foodRequestDTO.getDescription());

        food=foodRepository.save(food);
        if (foodRequestDTO.getFile() == null) {
            return food;
        }
        String fileType = foodRequestDTO.getFile().getContentType();
        assert fileType != null;
        fileType = fileType.substring(0, 5);

        FoodAvatar foodAvatar = new FoodAvatar();
        foodAvatar.setId(null);
        foodAvatar.setFileType(fileType);
        List<FoodAvatar> foodAvatar1=foodAvatarRepository.findByFood(food);
        foodAvatarRepository.delete(foodAvatar1.get(0));
        foodAvatar=foodAvatarRepository.save(foodAvatar);
        uploadAndSaveFoodImage(foodRequestDTO, foodAvatar);
        foodAvatar.setFood(food);
        return food ;
    }

    @Override
    public Food findByName(String name) {
        return foodRepository.findByName(name);
    }

    @Override
    public List<FoodAvatar> findByFood(Food food) {
        return foodAvatarRepository.findByFood(food);
    }

    private void uploadAndSaveFoodImage(FoodRequestDTO foodRequestDTO, FoodAvatar foodAvatar) {
        try {
            Map uploadResult = uploadService.uploadImage(foodRequestDTO.getFile(), uploadUtil.buildImageUploadParams(foodAvatar.getId(), uploadUtil.FOOD_IMAGE_UPLOAD_FOLDER, uploadUtil.ERROR_IMAGE_UPLOAD));
            String fileUrl = (String) uploadResult.get("secure_url");
            String fileFormat = (String) uploadResult.get("format");

            foodAvatar.setFileName(foodAvatar.getId() + "." + fileFormat);
            foodAvatar.setFileUrl(fileUrl);
            foodAvatar.setFileFolder(uploadUtil.FOOD_IMAGE_UPLOAD_FOLDER);
            foodAvatar.setCloudId(foodAvatar.getFileFolder() + "/" + foodAvatar.getId());
            foodAvatarRepository.save(foodAvatar);

        } catch (IOException e) {
            e.printStackTrace();
            throw new DataInputException("Upload hình ảnh thất bại");
        }
    }

    @Override
    public void softDelete(Long foodId) {
        foodRepository.softDelete(foodId);
    }

}
