package com.cg.domain.dto.customer;

import com.cg.domain.entity.CustomerAvatar;
import com.cg.domain.entity.LocationRegion;
import com.cg.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;


import javax.persistence.Entity;
import java.util.Date;
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
public class CustomerDTO {

    private Long id;

    private String fullName;

    private String email;

    private Date dob;
    private String phone;

    private Boolean status;
    private String fileName;
    private String fileFolder;
    private String fileUrl;

}
