package com.cg.service.show;

import com.cg.domain.entity.Show;
import com.cg.repository.ShowRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ShowServiceImpl implements IShowService {

    @Autowired
    private ShowRepository showRepository;

    @Override
    public List<Show> findAll() {
        return showRepository.findAll();
    }

    @Override
    public Show getById(Long id) {
        return showRepository.getById(id);
    }

    @Override
    public Optional<Show> findById(Long id) {
        return showRepository.findById(id);
    }

    @Override
    public Show save(Show show) {
        return showRepository.save(show);
    }

    @Override
    public void remove(Long id) {

    }
}
