package com.cg.service.foodAvatar;

import com.cg.domain.entity.FoodAvatar;
import com.cg.service.IGeneralService;

public interface IFoodAvatarService extends IGeneralService<FoodAvatar> {

}
