package com.cg.repository;

import com.cg.domain.dto.customer.CustomerDTO;
import com.cg.domain.dto.staff.StaffDTO;
import com.cg.domain.entity.Staff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface StaffRepository extends JpaRepository<Staff, Long> {
    @Query("SELECT NEW com.cg.domain.dto.staff.StaffDTO (" +
            "s.id, " +
            "s.fullName, " +
            "s.email, " +
            "s.dob, " +
            "s.phone, " +
            "s.locationRegion, " +
            "s.staffAvatar.id," +
            "sa.fileName, " +
            "sa.fileFolder, " +
            "sa.fileUrl " +
            ") " +
            "FROM Staff AS s " +
            "JOIN StaffAvatar AS sa " +
            "ON sa = s.staffAvatar " +
            "WHERE s.deleted = false "
    )
    List<StaffDTO> findAllStaffResponseDTO();
    @Query("SELECT NEW com.cg.domain.dto.staff.StaffDTO (" +
            "s.id, " +
            "s.fullName, " +
            "s.email, " +
            "s.dob, " +
            "s.phone, " +
            "s.locationRegion, " +
            "s.staffAvatar.id," +
            "sa.fileName, " +
            "sa.fileFolder, " +
            "sa.fileUrl " +
            ") " +
            "FROM Staff AS s " +
            "JOIN StaffAvatar AS sa " +
            "ON sa = s.staffAvatar WHERE s.id = ?1 "
    )
    StaffDTO findStaffResponseDTOById(Long id);

    Boolean existsStaffByEmailAndIdNot(String email,Long id);
    Boolean existsStaffByEmail(String email);

    @Modifying
    @Query("UPDATE Staff AS s SET s.deleted = true WHERE s.id = :staffId")
    void softDelete(@Param("staffId") long staffId);

}
