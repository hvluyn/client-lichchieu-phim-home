package com.cg.service.bill;

import com.cg.domain.entity.Bill;
import com.cg.repository.BillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class BillServiceImpl implements IBillService {

    @Autowired
    private BillRepository billRepository;

    @Override
    public List<Bill> findAll() {
        return billRepository.findAll();
    }

    @Override
    public Bill getById(Long id) {
        return billRepository.getById(id);
    }

    @Override
    public Optional<Bill> findById(Long id) {
        return billRepository.findById(id);
    }



    @Override
    public Bill save(Bill bill) {
        return billRepository.save(bill);
    }

    @Override

   public void remove(Long id) {

    }
}
