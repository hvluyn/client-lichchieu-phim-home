package com.cg;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpbModulesCineStarDashboardApiApplication {

    private static final Logger logger = LoggerFactory.getLogger(SpbModulesCineStarDashboardApiApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(SpbModulesCineStarDashboardApiApplication.class, args);
        logger.info("Cine Star Application Started........");
    }

}
