package com.cg.domain.dto.movie;

import com.cg.domain.entity.CategoryMovie;
import com.cg.domain.entity.Country;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
public class MovieDTO {

    private Long id;

    private String nameMovie;

    private String description;


    private Long duration;

    private Date startDay;


    private CategoryMovie categoryMovie;


    private Country country;

    private String idAvatar;
    private String fileName;
    private String fileFolder;
    private String fileUrl;
}
