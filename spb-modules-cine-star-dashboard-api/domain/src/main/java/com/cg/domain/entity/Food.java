package com.cg.domain.entity;

import com.cg.domain.dto.food.FoodDTO;
import com.cg.domain.dto.food.FoodRequestDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Accessors(chain = true)
public class Food extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false,name = "food_name")
    private String name;

    private String description;

    private BigDecimal price;

    private Long quantity;
    public FoodDTO toFoodDTO(Food food) {
        return new FoodDTO()
                .setId(id)
                .setName(name)
                .setDescription(description)
                .setPrice(price);
    }

}
