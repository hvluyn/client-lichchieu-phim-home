package com.cg.service.cartFoodItem;

import com.cg.domain.entity.CartFoodItem;
import com.cg.repository.CartFoodItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CartFoodItemServiceImpl implements ICartFoodItemService {

    @Autowired
    private CartFoodItemRepository cartFoodItemRepository;

    @Override
    public List<CartFoodItem> findAll() {
        return cartFoodItemRepository.findAll();
    }

    @Override
    public CartFoodItem getById(Long id) {
        return cartFoodItemRepository.getById(id);
    }

    @Override
    public Optional<CartFoodItem> findById(Long id) {
        return cartFoodItemRepository.findById(id);
    }



    @Override
    public CartFoodItem save(CartFoodItem cartFoodItem) {
        return cartFoodItemRepository.save(cartFoodItem);
    }

    @Override

   public void remove(Long id) {

    }
}
