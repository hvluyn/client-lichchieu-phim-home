package com.cg.service.categoryMovie;

import com.cg.domain.entity.CategoryMovie;
import com.cg.service.IGeneralService;

public interface ICategoryMovieService extends IGeneralService<CategoryMovie> {

}
