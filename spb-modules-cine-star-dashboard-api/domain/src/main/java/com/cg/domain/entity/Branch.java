//package com.cg.domain.entity;
//
//import lombok.AllArgsConstructor;
//import lombok.Getter;
//import lombok.NoArgsConstructor;
//import lombok.Setter;
//import lombok.experimental.Accessors;
//
//import javax.persistence.*;
//
//@NoArgsConstructor
//@AllArgsConstructor
//@Getter
//@Setter
//@Entity
//@Accessors(chain = true)
//public class Branch {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//
//    @Column(nullable = false,name = "branch_name")
//    private String name;
//
//    @OneToOne
//    @JoinColumn(name = "locationRegion_id",referencedColumnName = "id",nullable = false)
//    private LocationRegion locationRegion;
//
//}
