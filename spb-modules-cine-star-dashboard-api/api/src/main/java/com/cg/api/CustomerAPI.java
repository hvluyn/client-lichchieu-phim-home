package com.cg.api;

import com.cg.domain.dto.customer.CustomerDTO;
import com.cg.domain.entity.Customer;
import com.cg.exception.DataInputException;
import com.cg.service.customer.ICustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/customers")
public class CustomerAPI {

    @Autowired
    private ICustomerService customerService;

    @GetMapping
    public ResponseEntity<?> getAllCustomers() {
        List<CustomerDTO> customers= customerService.findAllCustomerResponseDTO();
        return new ResponseEntity<>(customers,HttpStatus.OK);
    }

    @PatchMapping ("/{id}")
    public ResponseEntity<?> updateStatusCustomersById(@PathVariable Long id) {
        Optional<Customer> customerOptional=customerService.findById(id);
        if(!customerOptional.isPresent()){
            throw new DataInputException("Thao tác không thành công, khách hàng không tồn tại");
        }
        Customer customer=customerOptional.get();
        if(customer.getStatus()){
            customer.setStatus(false);
        }else {
            customer.setStatus(true);
        }
        customerService.save(customer);
        CustomerDTO customerDTO= customerService.findCustomerResponseDTOById(id);
        return new ResponseEntity<>(customerDTO,HttpStatus.OK);
    }
}
