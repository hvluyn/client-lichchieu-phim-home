package com.cg.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity(name = "shows")
@Accessors(chain = true)
public class Show {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "showtime_id",referencedColumnName = "id",nullable = false)
    private ShowTime showtime;

    @ManyToOne
    @JoinColumn(name = "chair_id",referencedColumnName = "id",nullable = false)
    private Chair chair;

    private String status;
}
