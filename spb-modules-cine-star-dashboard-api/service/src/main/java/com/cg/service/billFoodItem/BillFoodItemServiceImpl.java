package com.cg.service.billFoodItem;

import com.cg.domain.entity.BillFoodItem;
import com.cg.repository.BillFoodItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class BillFoodItemServiceImpl implements IBillFoodItemService {

    @Autowired
    private BillFoodItemRepository billFoodItemRepository;

    @Override
    public List<BillFoodItem> findAll() {
        return billFoodItemRepository.findAll();
    }

    @Override
    public BillFoodItem getById(Long id) {
        return billFoodItemRepository.getById(id);
    }

    @Override
    public Optional<BillFoodItem> findById(Long id) {
        return billFoodItemRepository.findById(id);
    }



    @Override
    public BillFoodItem save(BillFoodItem billFoodItem) {
        return billFoodItemRepository.save(billFoodItem);
    }

    @Override

   public void remove(Long id) {

    }
}
