package com.cg.service.cartTicketItem;

import com.cg.domain.entity.CartTicketItem;
import com.cg.service.IGeneralService;

public interface ICartTicketItemService extends IGeneralService<CartTicketItem> {

}
