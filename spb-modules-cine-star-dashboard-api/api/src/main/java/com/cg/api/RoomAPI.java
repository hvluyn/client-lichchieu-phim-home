package com.cg.api;

import com.cg.domain.dto.movie.MovieDTO;
import com.cg.domain.entity.Room;
import com.cg.domain.entity.Theater;
import com.cg.exception.DataInputException;
import com.cg.service.room.IRoomService;
import com.cg.service.showtime.IShowtimeService;
import com.cg.service.theater.ITheaterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/rooms")
public class RoomAPI {
    @Autowired
    private IRoomService roomService ;
    @Autowired
    private ITheaterService theaterService ;

    @GetMapping("/{id}")
    public ResponseEntity<?> getAllRoomByIdTheater(@PathVariable Long id) {
        Optional<Theater> optionalTheater=theaterService.findById(id);
        if(!optionalTheater.isPresent()){
            throw  new DataInputException("Rạp không tồn tại, vui lòng kiểm tra lại");
        }
        List<Room> rooms= roomService.findAllByTheater(optionalTheater.get());
        return new ResponseEntity<>(rooms, HttpStatus.OK);
    }
}
