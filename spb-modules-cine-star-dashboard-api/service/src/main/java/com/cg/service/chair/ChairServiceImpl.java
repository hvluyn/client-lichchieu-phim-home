package com.cg.service.chair;

import com.cg.domain.entity.Chair;
import com.cg.repository.ChairRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ChairServiceImpl implements IChairService {

    @Autowired
    private ChairRepository chairRepository;

    @Override
    public List<Chair> findAll() {
        return chairRepository.findAll();
    }

    @Override
    public Chair getById(Long id) {
        return chairRepository.getById(id);
    }

    @Override
    public Optional<Chair> findById(Long id) {
        return chairRepository.findById(id);
    }



    @Override
    public Chair save(Chair chair) {
        return chairRepository.save(chair);
    }

    @Override

   public void remove(Long id) {

    }
}
