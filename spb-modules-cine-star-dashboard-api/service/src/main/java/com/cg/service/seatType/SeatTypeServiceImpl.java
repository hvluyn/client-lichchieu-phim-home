package com.cg.service.seatType;

import com.cg.domain.entity.SeatType;
import com.cg.repository.SeatTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class SeatTypeServiceImpl implements ISeatTypeService{

    @Autowired
    private SeatTypeRepository seatTypeRepository;

    @Override
    public List<SeatType> findAll() {
        return seatTypeRepository.findAll();
    }

    @Override
    public SeatType getById(Long id) {
        return null;
    }

    @Override
    public Optional<SeatType> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public SeatType save(SeatType seatType) {
        return null;
    }

    @Override
    public void remove(Long id) {

    }
}
